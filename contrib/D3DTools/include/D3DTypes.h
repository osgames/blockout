//***************************************************
// Class D3DObject
// Various type used in the D3Dtools library
// Author: JL Pons (2002)
//***************************************************

// Type of D3DObject

#define _3DSPRITE    1
#define _3DOBJ       2
#define _2DSPRITE    3
#define _3DBLEND     4

// Format for D3DObject

#define MYVERTEX_FORMAT   (D3DFVF_XYZ    | D3DFVF_NORMAL  | D3DFVF_TEX1)
#define MYSVERTEX_FORMAT  (D3DFVF_XYZ    | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define MYSSVERTEX_FORMAT (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define MYBVERTEX_FORMAT  (D3DFVF_XYZ    | D3DFVF_NORMAL | D3DFVF_TEX1 | D3DFVF_TEX2 | D3DFVF_TEX3)

//-----------------------------------------------------------------------------
// MYVERTEX_FORMAT format
//-----------------------------------------------------------------------------
typedef struct  {

  float x;
  float y;
  float z;
  float nx;
  float ny;
  float nz;
  float tu;
  float tv;

} MYD3DVERTEX;

//-----------------------------------------------------------------------------
// MYSVERTEX_FORMAT format
//-----------------------------------------------------------------------------
typedef struct  {

  float x;
  float y;
  float z;
  DWORD c;   // Not used
  float tu;
  float tv;

} MYD3DSVERTEX;

//-----------------------------------------------------------------------------
// MYSSVERTEX_FORMAT format
//-----------------------------------------------------------------------------
typedef struct { 

  float x; 
  float y; 
  float z; 
  float rhw; 
  DWORD c; 
  float tu; 
  float tv; 

} MYD3DSSVERTEX; 

//-----------------------------------------------------------------------------
// MYBVERTEX_FORMAT format
//-----------------------------------------------------------------------------
typedef struct { 

  float x; 
  float y; 
  float z; 
  float nx;
  float ny;
  float nz;
  float tu;             // Stage 0 
  float tv;             // Stage 0
  float tuAlphaBlend;   // Stage 1
  float tvAlphaBlend;   // Stage 1
  float tuDiff;         // Stage 2
  float tvDiff;         // Stage 2

} MYD3DBVERTEX; 

//-----------------------------------------------------------------------------
// Data structure a 3D objects
//-----------------------------------------------------------------------------
typedef struct {

  int						          type;       // type of object
  LPDIRECT3DVERTEXBUFFER8 hp;         // vertices buffer
  DWORD					          nbp;        // Number of vertex
  DWORD                   nbi;        // Number of triangle
  LPDIRECT3DINDEXBUFFER8  ind;        // Indices
  DWORD					          ambient;    // Ambient light
  D3DXVECTOR3				      bary;       // Object barycentre
  float					          height;     // Height of _3DSPRITE
  float					          width;      // Width  of _3DSPRITE
  DWORD                   color;      // Color of _2DSRITE
  D3DTexture				     *map;        // Handle to the texture

} _3DObject;


// Null vector
extern const D3DXVECTOR3 vNull;

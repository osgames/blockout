//***************************************************
// Class D3DTexture
// Class for D3DTexture (DirectX 8.0)
// Author: JL Pons (2002)
//***************************************************
#include <stdio.h>
#include <math.h>
#include <D3DX8.h>
#include <CImage.h>


#define MAX_TEXTURE 100
#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#define SAFE_DELETE(p)      { if(p) { delete (p); (p)=NULL; } }

class D3DTexture {

public:

	LPDIRECT3DTEXTURE8		hmap;       // Handle to map

	char					*diff_name; // Name of the difuse map
	char					*alpha_name;// Name of the alpha map

	BOOL					trans;      // Black transparent
	BOOL					bilinear;   // Bilinear filtering
	BOOL					tile;       // UV tiling
	BOOL					mip_map;    // Mip mapping

	// Contructors (Never call, use static function for creation)
    D3DTexture();
	~D3DTexture();

	// Load textures from image files
	BOOL load_texture(char *filename,char *err_msg);
	BOOL load_texture_alpha(char *diff_name,char *alpha_name,char *err_msg);
	BOOL load_texture_mipmap(char *basename,char *err_msg);

	// Create a texture from file (Return NULL when failed)
	static D3DTexture *CreateTextureFromFile(LPDIRECT3DDEVICE8 pd3dDevice,char *diff_name,char *alpha_name,char *err_msg);

  // Create a texture from a LPDIRECT3DTEXTURE8
  static D3DTexture *D3DTexture::CreateTexture(LPDIRECT3DTEXTURE8 map,BOOL hasAlpha);

  // Set the search path for texture
  static void D3DTexture::SetTexturePath(char *pathName);

	// Prepare 3D device to render the texture
	HRESULT PrepareForRendering(LPDIRECT3DDEVICE8 m_pd3dDevice);

	// Returns true if this texture has alpha channel
	BOOL hasAlpha();

	static HRESULT Destroy();
	static char    *GetInfos();
	static HRESULT FillSurfaceFrom24Bit(LPDIRECT3DSURFACE8 pSurf,CImage *img);

};
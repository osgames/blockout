/*
 	File:        Menu.h
  Description: Contains menu pages.
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#ifndef MENUPAGEH
#define MENUPAGEH

#define STRICT
#include <math.h>
#include <D3DX8.h>
#include <D3DApp.h>
#include <D3DFont.h>
#include <D3DUtil.h>
#include <DXUtil.h>
#include <D3DObject.h>
#include "PolyCube.h"

class Menu;

#include "EditControl.h"

#define DECLARE(className,exts) class className: public MenuPage {                \
                                public:                                           \
                                  className() {};                                 \
                                  void Prepare(int iParam,void *pParam);          \
                                  void Render(LPDIRECT3DDEVICE8 pd3dDevice);      \
                                  int  Process(BYTE *keys,float fTime);           \
                                private:                                          \
                                  exts                                            \
                                }

// Super Class
class MenuPage {
public:

  // Init parent
  void SetParent(Menu *parent) {mParent=parent;};

  // Overrides
  virtual void Prepare(int iParam,void *pParam) = NULL;
  virtual void Render(LPDIRECT3DDEVICE8 pd3dDevice) = NULL;
  virtual int  Process(BYTE *keys,float fTime) = NULL;
  
 protected:

   void ProcessDefault(BYTE *keys,float fTime) {

    if(keys[VK_DOWN]) {
      selItem++;
      if(selItem>=nbItem) selItem=0;
      keys[VK_DOWN] = 0;
    }
     
    if(keys[VK_UP]) {
      selItem--;
      if(selItem<0) selItem=nbItem-1;
      keys[VK_UP] = 0;
    }

  }

  int selItem;
  int nbItem;
  Menu *mParent;

};

// Sub classes
DECLARE(PageMainMenu,
  float startWriteTime;
);

DECLARE(PageStartGame,);
DECLARE(PageChooseSetup,);

DECLARE(PageChangeSetup,
  void ProcessKey(int keyCode);
);

DECLARE(PageHallOfFame,
  SCOREREC   allScore[10];
  BOOL       editMode;
  char       editText[11];
  int        editPos;
  BOOL       editCursor;
  BOOL       editMaj;
  SCOREREC  *editScore;
  float      startEditTime;
  PLAYER_INFO pInfo;

  void ProcessEdit(BYTE *keys,float fTime);
public:
  PLAYER_INFO *GetPlayer();
);

DECLARE(PageOptions,);

DECLARE(PageGSOptions,
  int ProcessKey(int keyCode);
);

DECLARE(PageScoreDetails,
  SCOREREC  *score;
  int rank;
  int   pageState;
  float startTime;
  char  errMsg[512];
);

DECLARE(PageControls,
    D3DVIEWPORT8 blockView;
    D3DXMATRIX   blockProj;
    D3DObject  pitBack;
    D3DXMATRIX matBlock[3];
    PolyCube   block[3];
    float      startTime;
    float      cubeSide;
    int        rotPos;
    BOOL       rotMode;
    BOOL       editMode;
    float      startEditTime;
    BOOL       cursorVisible;

    void ProcessMenuDis(BYTE *keys);
    void ProcessEdit(BYTE *keys);
    void ProcessAnimation(float fTime);
    void Place(PolyCube *obj,D3DXMATRIX *mat,D3DXMATRIX *matR,float x,float y,float z);
    void RenderKey(int x,int y,char k,int pos);
  public:
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice);
    void InvalidateDeviceObjects();
);

DECLARE(PageHttp,
  EditControl edit;
  char *Wrap(char *str);
);

DECLARE(PageHallOfFameOnLine,
  SCOREREC   allScore[100];
  int        startPos;
  int        downloadState;
  char       errMsg[512];
  char      *pPos;
  PLAYER_INFO pInfo;

  void  FetchNextLine();
  char *FetchNextField();
  void  ParseScore(SCOREREC *s);
  void  DownloadScore();
public:
  PLAYER_INFO *GetPlayer();
);

DECLARE(PageCredits,
   D3DVIEWPORT8 blockView;
   D3DXMATRIX   blockProj;
   D3DXMATRIX   matBlock;
   PolyCube     block;
   float        startTime;
  public:
   HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice);
   void InvalidateDeviceObjects();
);

#endif /* MENUPAGEH */

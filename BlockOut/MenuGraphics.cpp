/*
 	File:        MenuGraphics.cpp
  Description: Menu graphics management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Menu.h"

// ---------------------------------------------------------------------

void Menu::InitGraphics() {

  startMenuTime = 0.0f;

  // Intitial rotation matrix of "BLOCKOUT" letters (Startup demo)
  
  blOrgMatrix = D3DXMATRIX( 1.0f , 0.0f , 0.0f , 0.0f ,
                            0.0f ,-1.0f , 0.0f , 0.0f ,
                            0.0f , 0.0f ,-1.0f , 0.0f ,
                            0.0f , 0.0f , 0.0f , 1.0f );
  
  // Origin and end position of "BLOCKOUT" letters (Startup demo)

  // B
  startPos[0] = v(0.7f,-0.5f,0.0f);
  endPos[0] = v(0.65f,0.9f ,3.0f);

  // L
  startPos[1] =  v(0.25f,-0.5f,0.0f);
  endPos[1] =  v(0.25f,1.1f,3.0f);

  // O
  startPos[2] =  v(0.0f,-0.5f,0.0f);
  endPos[2] =  v(-0.15f,1.1f,3.0f);

  // C
  startPos[3] =  v(-0.25f,-0.5f,0.0f);
  endPos[3] =  v(-0.55f,1.1f,3.0f);

  // K
  startPos[4] =  v(-0.7f,-0.5f,0.0f);
  endPos[4]= v(-1.25f,0.9f,3.0f);

  // O
  startPos[5] =  v(0.25f,-0.5f,0.0f);
  endPos[5] =  v(0.25f,0.4f,3.0f);

  // U
  startPos[6] =  v(0.0f,-0.5f,0.0f);
  endPos[6] =  v(-0.15f,0.4f,3.0f);

  // T
  startPos[7] =  v( -0.25f,-0.5f,0.0f);
  endPos[7] =  v(-0.55f,0.4f,3.0f);

  // II
  startPos[8] =  v( -0.5f,-0.5f,0.0f);
  endPos[8] =  v(-1.35f,0.4f,3.0f);

  for(int i=0;i<BLLETTER_NB;i++) {
    isAdded[i] = FALSE;
    isStarted[i] = FALSE;
  }
  animEnded = FALSE;
  menuEscaped = FALSE;
  m_pd3dDevice = NULL;
  blLetters = new PolyCube[BLLETTER_NB];

}

// ---------------------------------------------------------------------

void Menu::FullRepaint() {
  // For the 2 back buffers
  fullRepaint = 2;
}

// ---------------------------------------------------------------------

HRESULT Menu::Create(LPDIRECT3DDEVICE8 pd3dDevice) {

  char err_msg[1024];
  HRESULT hr;

  m_pd3dDevice = pd3dDevice;

  // Get back buffer dimemsion
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  pBackBuffer->Release();
  scrWidth = desc.Width;
  scrHeight = desc.Height;

  // Menu viewport
  menuView.X      = 0;
  menuView.Y      = 0;
  menuView.Width  = desc.Width;
  menuView.Height = desc.Height;
  menuView.MinZ   = 0.0f;
  menuView.MaxZ   = 1.0f;

  // Projection matrix
  FLOAT fAspectRatio = (FLOAT)desc.Width / (FLOAT)desc.Height;
  D3DXMatrixPerspectiveFovRH( &matProj, D3DXToRadian(60.0f), fAspectRatio, 0.1f, 10.0f );

  // Initialiase letters
  hr = InitBlLetters();

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::InitBlLetters failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
    return E_FAIL;
  }

  // Initialiase grid
  hr = theGrid.Create(pd3dDevice);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateGrid failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
    return E_FAIL;
  }

  // Background
  int x1 = fround( (float)desc.Width  * 0.13f );
  int y1 = fround( (float)desc.Height * 0.41f );
  int x2 = fround( (float)desc.Width  * 0.87f );
  int y2 = fround( (float)desc.Height * 0.985f );
  hr = background.Create2DSprite(pd3dDevice,"images\\menuback.png","none",x1,y1,x2,y2);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateBackground failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
    return E_FAIL;
  }

  hr = background2.Create2DSprite(pd3dDevice,"images\\menucredits.png","none",x1,y1,x2,y2);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateBackground failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
    return E_FAIL;
  }

  // Font
  wFont = fround( (float)desc.Width  * 0.0205f );
  hFont = fround( (float)desc.Height * 0.0449f );
  hr = font.Create2DSprite(pd3dDevice,"images\\menufont.png","none",0,0,0,0);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateFont failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;
  }
  hr = font2.Create2DSprite(pd3dDevice,"images\\menufont2.png","none",0,0,0,0);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateFont failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;
  }

  // onLine logo
  hr = onlineLogo.Create2DSprite(pd3dDevice,"images\\online.png","images\\onlinea.png",0,0,0,0);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateLogo failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;
  }

  x1 = fround( 0.148f * (float)desc.Width );
  y1 = fround( 0.413f * (float)desc.Height );
  x2 = fround( 0.232f * (float)desc.Width );
  y2 = fround( 0.524f * (float)desc.Height );
  onlineLogo.UpdateSprite(x1,y1,x2,y2,0.0f,0.0f,1.0f,1.0f);

  // Controls menu page
  hr = controlsPage.Create(pd3dDevice);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateControlsPage failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;
  }

  // Credits page
  hr = creditsPage.Create(pd3dDevice);

  if(FAILED(hr)) {
    sprintf(err_msg,"Menu::CreateCreditsPage failed:%s",GetDDErrorMsg(hr));
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;
  }

  FullRepaint();

  return S_OK;
}
// ---------------------------------------------------------------------

void Menu::RenderChar(int x,int y,int w,int h,BOOL selected,char c) {

  BOOL ok = FALSE;
  int  fnt = 1;

  float sX = 0.0f; 
  float sY = 0.0f; 
  float eX = 0.0f; 
  float eY = 0.0f;

  if( !selected && c==' ' ) {
    // Do not render unselected space
    return;
  }

  // Space
  if( c==' ' ) {
    sX = 12.0f * 0.0638f + 0.005f; 
    sY = 0.2930f; 
    eX = 13.0f * 0.0638f + 0.003f; 
    eY = 0.3828f;
    ok = TRUE;
  }

  // Mapping
  if( c>='A' && c<='O' ) {
    sX = (float)(c-'A')   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = (float)(c-'A'+1) * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
  }

  if( c>='P' && c<='Z' ) {
    sX = (float)(c-'P')   * 0.0638f + 0.005f; 
    sY = 0.0977f; 
    eX = (float)(c-'P'+1) * 0.0638f + 0.003f; 
    eY = 0.1875f;
    ok = TRUE;
  }

  if( c>='a' && c<='o' ) {
    sX = (float)(c-'a')   * 0.0638f + 0.005f;
    sY = 0.1953f; 
    eX = (float)(c-'a'+1) * 0.0638f + 0.003f;
    eY = 0.2852f;
    ok = TRUE;
  }

  if( c>='p' && c<='z' ) {
    sX = (float)(c-'p')   * 0.0638f + 0.005f; 
    sY = 0.2930f; 
    eX = (float)(c-'p'+1) * 0.0638f + 0.003f; 
    eY = 0.3828f;
    ok = TRUE;
  }

  if( c>='0' && c<='9' ) {
    sX = (float)(c-'0')   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = (float)(c-'0'+1) * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c=='[' ) {
    sX = 10.0f   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = 11.0f * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c==']' ) {
    sX = 11.0f   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = 12.0f * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c==':' ) {
    sX = 12.0f   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = 13.0f * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c=='.' ) {
    sX = 13.0f   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = 14.0f * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c==',' ) {
    sX = 14.0f   * 0.0638f + 0.005f; 
    sY = 0.3906f; 
    eX = 15.0f * 0.0638f + 0.003f; 
    eY = 0.4805f;
    ok = TRUE;
  }

  if( c=='-' ) {
    sX = 11.0f   * 0.0638f + 0.005f; 
    sY = 0.2930f; 
    eX = 12.0f * 0.0638f + 0.003f; 
    eY = 0.3828f;
    ok = TRUE;
  }

  if( c=='+' ) {
    sX = 11.0f   * 0.0638f + 0.005f; 
    sY = 0.0977f; 
    eX = 12.0f * 0.0638f + 0.003f; 
    eY = 0.1875f;
    ok = TRUE;
  }

  if( c=='/' ) {
    sX = 12.0f   * 0.0638f + 0.005f; 
    sY = 0.0977f; 
    eX = 13.0f * 0.0638f + 0.003f; 
    eY = 0.1875f;
    ok = TRUE;
  }

  if( c=='{' ) {
    sX = 13.0f   * 0.0638f + 0.005f; 
    sY = 0.0977f; 
    eX = 14.0f * 0.0638f + 0.003f; 
    eY = 0.1875f;
    ok = TRUE;
  }

  if( c=='@' ) {
    sX = 14.0f   * 0.0638f + 0.005f; 
    sY = 0.0977f; 
    eX = 15.0f * 0.0638f + 0.003f; 
    eY = 0.1875f;
    ok = TRUE;
  }

  if( c=='\'' ) {
    sX = 14.0f   * 0.0638f + 0.005f; 
    sY = 0.2930f; 
    eX = 15.0f * 0.0638f + 0.003f; 
    eY = 0.3828f;
    ok = TRUE;
  }

  if( c=='#' ) {
    sX = 12.0f * 0.0638f + 0.005f; 
    sY = 0.2930f; 
    eX = 13.7f * 0.0638f + 0.003f; 
    eY = 0.3828f;
    ok = TRUE;
  }

  if( c=='!' ) {
    sX = 0.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 1.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='$' ) {
    sX = 1.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 2.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='%' ) {
    sX = 2.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 3.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='&' ) {
    sX = 3.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 4.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='*' ) {
    sX = 4.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 5.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='(' ) {
    sX = 5.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 6.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c==')' ) {
    sX = 6.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 7.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='_' ) {
    sX = 7.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 8.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c=='?' ) {
    sX = 8.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 9.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( c==';' ) {
    sX = 9.0f   * 0.0638f + 0.005f;
    sY = 0.0f; 
    eX = 10.0f * 0.0638f + 0.003f;
    eY = 0.0898f;
    ok = TRUE;
    fnt = 2;
  }

  if( !ok ) return;

  if(selected) {
    sY += 0.508f; 
    eY += 0.508f;
  }

  if( fnt==1 ) {
    font.UpdateSprite(x,y,x+w,y+h,sX,sY,eX,eY);
    font.Render(m_pd3dDevice);
  } else {
    font2.UpdateSprite(x,y,x+w,y+h,sX,sY,eX,eY);
    font2.Render(m_pd3dDevice);
  }

}

// ---------------------------------------------------------------------

void Menu::RenderTitle(char *title) {

  int  lgth = strlen(title);
  int  nwFont = fround((float)wFont*1.1f);
  int  nhFont = fround((float)hFont*1.1f);
  int  x1 = (scrWidth - (nwFont * lgth))/2;
  int  y1 = fround( 0.43f * (float)scrHeight );

  for(int i=0;i<lgth;i++,x1+=nwFont) {
    RenderChar(x1,y1,nwFont,nhFont,FALSE,title[i]);
  }

}

// ---------------------------------------------------------------------

void Menu::RenderText(int x,int y,BOOL selected,char *text) {

  float startLine   = 0.515f;
  float startColumn = 0.15f;
  float lineHeight  = 0.045f;

  int   lgth = strlen(text);
  int   x1 = fround( startColumn*(float)scrWidth ) + x*wFont;
  int   y1 = fround( (startLine+y*lineHeight)*(float)scrHeight );

  for(int i=0;i<lgth;i++,x1+=wFont) {
    RenderChar(x1,y1,wFont,hFont,selected,text[i]);
  }

}

// ---------------------------------------------------------------------

void Menu::RenderVersion() {

  float startColumn = 0.15f;
  float lineHeight  = 0.045f;

  int   x1 = fround( startColumn*(float)scrWidth ) + 30*wFont;;
  int   y1 = fround( 0.43f * (float)scrHeight );

  RenderChar(x1,y1,wFont,hFont,FALSE,'v');
  x1+=wFont;
  for(int i=0;i<3;i++,x1+=wFont) {
    RenderChar(x1,y1,wFont,hFont,FALSE,APP_VERSION[i+9]);
  }

}

// ---------------------------------------------------------------------

void Menu::Render() {

  if( !m_pd3dDevice )
    return;

  // Disable texture, alpha and z buff
  m_pd3dDevice->SetTexture( 0, NULL );
  m_pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  m_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_DISABLE );
  m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
  m_pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
  m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );	
  m_pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);

  // Set Projection and viewport
  m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
  m_pd3dDevice->SetViewport(&menuView);

  // Clear and render the background
  if( fullRepaint>0 || !animEnded ) {

    // Menu startup demo
    m_pd3dDevice->Clear( 0L, NULL, D3DCLEAR_TARGET, 0L, 1.0f, 0L );
    theGrid.Render(m_pd3dDevice);

    for(int i=0;i<BLLETTER_NB;i++) {
      if( !isAdded[i] ) {
        m_pd3dDevice->SetTransform( D3DTS_WORLD, blMatrix+i );
        blLetters[i].Render(m_pd3dDevice,FALSE,FALSE);
      }
    }

    fullRepaint--;

  }

  if( animEnded ) {
    if( selPage != &creditsPage )
      background.Render(m_pd3dDevice);
    else
      background2.Render(m_pd3dDevice);
    selPage->Render(m_pd3dDevice);
    // Restore full viewport
    m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
    m_pd3dDevice->SetViewport(&menuView);
  }

}

// ---------------------------------------------------------------------

void Menu::InvalidateDeviceObjects() {
  controlsPage.InvalidateDeviceObjects();
  creditsPage.InvalidateDeviceObjects();
  theGrid.InvalidateDeviceObjects();
  background.InvalidateDeviceObjects();
  background2.InvalidateDeviceObjects();
  font.InvalidateDeviceObjects();
  font2.InvalidateDeviceObjects();
  onlineLogo.InvalidateDeviceObjects();
  for(int i=0;i<BLLETTER_NB;i++)
    blLetters[i].InvalidateDeviceObjects();
}

// ---------------------------------------------------------------------

void Menu::ResetAnim(float fTime) {

  startMenuTime = fTime;
  for(int i=0;i<BLLETTER_NB;i++) {
    isAdded[i] = FALSE;
    isStarted[i] = FALSE;
  }
  theGrid.Clear();
  animEnded = FALSE;

}

// ---------------------------------------------------------------------

void Menu::ProcessAnim(float fTime) {

  if( startMenuTime==0.0f ) {
    // Menu startup
    startMenuTime = fTime;
  }

  // Compute letters transformation matrix
  float angle = 0.0f;
  float xPos = 0.0f;
  float yPos = 0.0f;
  float zPos = 0.0f;
  D3DXMATRIX t;
  D3DXMATRIX r;
  float mTime = (fTime - startMenuTime);

  for(int i=0;i<BLLETTER_NB;i++) {

    float delay = (float)i * 0.25f; // 250ms delay between letters

    if( mTime < delay ) {
      angle =  0.0f;
      xPos  =  startPos[i].x;
      yPos  =  startPos[i].y;
      zPos  =  startPos[i].z;
    } else if( mTime > ANIMTIME+delay ) {  

      animEnded = (i==BLLETTER_NB-1);
      if( animEnded ) FullRepaint();

      // Add the letter to the grid
      if( !isAdded[i] ) {

        BLOCKITEM c[MAX_CUBE];
        int nbCube;

        isAdded[i] = TRUE;
        int ox = fround( endPos[i].x*10.0f + 0.5f ) + GRID_WIDTH/2;
        int oy = fround( endPos[i].y*10.0f ) + GRID_HEIGHT/2;
        blLetters[i].CopyCube(c,&nbCube);
        for(int j=0;j<nbCube;j++) {
          theGrid.SetValue( ox + c[j].x , oy + c[j].y , 1);
        }
        if(!menuEscaped)
          soundManager->PlayTchh();

      }

    } else {

      if( (i==1 || i==5) && !isStarted[i] ) {
        soundManager->PlayWozz();
        isStarted[i] = TRUE;
      }

      float ratio = (mTime-delay)/ANIMTIME;
      angle = PI * ratio;
      xPos  = startPos[i].x + (endPos[i].x-startPos[i].x) * ratio;
      yPos  = startPos[i].y + (endPos[i].y-startPos[i].y) * ratio;
      zPos  = startPos[i].z + (endPos[i].z-startPos[i].z) * ratio;

    }

    D3DXMatrixTranslation(&t,xPos,yPos,zPos);
    D3DXMatrixRotationX(&r,angle);
    D3DXMatrixMultiply(blMatrix+i,&blOrgMatrix,&r);
    D3DXMatrixMultiply(blMatrix+i,blMatrix+i,&t);

  }


}

// ---------------------------------------------------------------------

HRESULT Menu::InitBlLetters() {

  HRESULT hr;

  VERTEX org = v(0.0f,0.0f,0.0f);

  // "B" letter
  blLetters[0].AddCube(0,0,0);
  blLetters[0].AddCube(1,0,0);
  blLetters[0].AddCube(2,0,0);
  blLetters[0].AddCube(3,0,0);
  blLetters[0].AddCube(4,0,0);
  blLetters[0].AddCube(5,0,0);
  blLetters[0].AddCube(0,1,0);
  blLetters[0].AddCube(4,1,0);
  blLetters[0].AddCube(0,2,0);
  blLetters[0].AddCube(4,2,0);
  blLetters[0].AddCube(0,3,0);
  blLetters[0].AddCube(1,3,0);
  blLetters[0].AddCube(2,3,0);
  blLetters[0].AddCube(3,3,0);
  blLetters[0].AddCube(4,3,0);
  blLetters[0].AddCube(1,4,0);
  blLetters[0].AddCube(4,4,0);
  blLetters[0].AddCube(1,5,0);
  blLetters[0].AddCube(4,5,0);
  blLetters[0].AddCube(1,6,0);
  blLetters[0].AddCube(2,6,0);
  blLetters[0].AddCube(3,6,0);
  blLetters[0].AddCube(4,6,0);
  blLetters[0].AddCube(5,6,0);
  hr = blLetters[0].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "L" letter
  blLetters[1].AddCube(0,0,0);
  blLetters[1].AddCube(1,0,0);
  blLetters[1].AddCube(2,0,0);
  blLetters[1].AddCube(2,1,0);
  blLetters[1].AddCube(2,2,0);
  blLetters[1].AddCube(2,3,0);
  blLetters[1].AddCube(2,4,0);
  hr = blLetters[1].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "O" letter
  blLetters[2].AddCube(0,0,0);
  blLetters[2].AddCube(1,0,0);
  blLetters[2].AddCube(2,0,0);
  blLetters[2].AddCube(0,1,0);
  blLetters[2].AddCube(2,1,0);
  blLetters[2].AddCube(0,2,0);
  blLetters[2].AddCube(2,2,0);
  blLetters[2].AddCube(0,3,0);
  blLetters[2].AddCube(2,3,0);
  blLetters[2].AddCube(0,4,0);
  blLetters[2].AddCube(1,4,0);
  blLetters[2].AddCube(2,4,0);
  hr = blLetters[2].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "C" letter
  blLetters[3].AddCube(0,0,0);
  blLetters[3].AddCube(1,0,0);
  blLetters[3].AddCube(2,0,0);
  blLetters[3].AddCube(0,1,0);
  blLetters[3].AddCube(2,1,0);
  blLetters[3].AddCube(2,2,0);
  blLetters[3].AddCube(0,3,0);
  blLetters[3].AddCube(2,3,0);
  blLetters[3].AddCube(0,4,0);
  blLetters[3].AddCube(1,4,0);
  blLetters[3].AddCube(2,4,0);
  hr = blLetters[3].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;


  // "K" letter
  blLetters[4].AddCube(0,0,0);
  blLetters[4].AddCube(1,0,0);
  blLetters[4].AddCube(4,0,0);
  blLetters[4].AddCube(5,0,0);
  blLetters[4].AddCube(1,1,0);
  blLetters[4].AddCube(4,1,0);
  blLetters[4].AddCube(2,2,0);
  blLetters[4].AddCube(4,2,0);
  blLetters[4].AddCube(3,3,0);
  blLetters[4].AddCube(4,3,0);
  blLetters[4].AddCube(2,4,0);
  blLetters[4].AddCube(4,4,0);
  blLetters[4].AddCube(1,5,0);
  blLetters[4].AddCube(4,5,0);
  blLetters[4].AddCube(0,6,0);
  blLetters[4].AddCube(1,6,0);
  blLetters[4].AddCube(4,6,0);
  blLetters[4].AddCube(5,6,0);
  hr = blLetters[4].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "O" letter
  blLetters[5].AddCube(0,0,0);
  blLetters[5].AddCube(1,0,0);
  blLetters[5].AddCube(2,0,0);
  blLetters[5].AddCube(0,1,0);
  blLetters[5].AddCube(2,1,0);
  blLetters[5].AddCube(0,2,0);
  blLetters[5].AddCube(2,2,0);
  blLetters[5].AddCube(0,3,0);
  blLetters[5].AddCube(2,3,0);
  blLetters[5].AddCube(0,4,0);
  blLetters[5].AddCube(1,4,0);
  blLetters[5].AddCube(2,4,0);
  hr = blLetters[5].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "U" letter
  blLetters[6].AddCube(0,0,0);
  blLetters[6].AddCube(1,0,0);
  blLetters[6].AddCube(2,0,0);
  blLetters[6].AddCube(0,1,0);
  blLetters[6].AddCube(2,1,0);
  blLetters[6].AddCube(0,2,0);
  blLetters[6].AddCube(2,2,0);
  blLetters[6].AddCube(0,3,0);
  blLetters[6].AddCube(2,3,0);
  blLetters[6].AddCube(0,4,0);
  blLetters[6].AddCube(2,4,0);
  hr = blLetters[6].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "T" letter
  blLetters[7].AddCube(1,0,0);
  blLetters[7].AddCube(1,1,0);
  blLetters[7].AddCube(1,2,0);
  blLetters[7].AddCube(1,3,0);
  blLetters[7].AddCube(0,4,0);
  blLetters[7].AddCube(1,4,0);
  blLetters[7].AddCube(2,4,0);
  hr = blLetters[7].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  // "II" letter
  blLetters[8].AddCube(0,0,0);
  blLetters[8].AddCube(1,0,0);
  blLetters[8].AddCube(2,0,0);
  blLetters[8].AddCube(3,0,0);
  blLetters[8].AddCube(4,0,0);
  blLetters[8].AddCube(1,1,0);
  blLetters[8].AddCube(1,2,0);
  blLetters[8].AddCube(3,1,0);
  blLetters[8].AddCube(3,2,0);
  blLetters[8].AddCube(0,3,0);
  blLetters[8].AddCube(1,3,0);
  blLetters[8].AddCube(2,3,0);
  blLetters[8].AddCube(3,3,0);
  blLetters[8].AddCube(4,3,0);
  hr = blLetters[8].Create(m_pd3dDevice,0.1f,org,FALSE);
  if(FAILED(hr)) return hr;

  return S_OK;
}

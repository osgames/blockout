/*
 	File:        Pit.cpp
  Description: Pit management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Pit.h"

#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }


//-----------------------------------------------------------------------------

Pit::Pit() {

  gridVbf = NULL;
  gridIdx = NULL;
  backVbf = NULL;
  backIdx = NULL;
  sideVbf = NULL;
  sideIdx = NULL;
 	cubeVbf = NULL;
	cubeIdx = NULL;
 	lcubeVbf = NULL;
  for(int i=0;i<12;i++)
	  lcubeIdx[i] = NULL;
  width = 0;
  height = 0;
  depth = 0;
  mSize = 0;
  style = 0;
  nbGridVertex = 0;
  nbGridIndex = 0;
  cubeSide = 0.0f;
  matrix = NULL;
  orderMatrix = NULL;
  inited = FALSE;
  backTexture = NULL;
  cubeTexture = NULL;
  for(int i=0;i<NBTCUBE;i++) cubeTVbf[i]=NULL;

}

//-----------------------------------------------------------------------------

void Pit::SetDimension(int gWidth,int gHeight,int gDepth) {

  width  = gWidth;
  height = gHeight;
  depth  = gDepth;

  // Init pit matrix
  if( matrix ) {
    free(matrix);
    matrix = NULL;
  }
  if( orderMatrix ) {
    free(orderMatrix);
    orderMatrix = NULL;
  }

  mSize = width * height * depth;
  if( mSize == 0 ) {
    // Invalid dimension
    return;
  }
  matrix = (int *)malloc( mSize*sizeof(int) );
  Clear();
  InitOrderMatrix();

}

//-----------------------------------------------------------------------------

BOOL Pit::IsEmpty() {

  int i = 0;
  BOOL empty = TRUE;
  while(empty && i<depth) {
    empty = IsLineEmpty(i);
    i++;
  }
  return empty;

}

//-----------------------------------------------------------------------------

int Pit::RemoveLines() {

  int nbRemoved = 0;
  int k=depth-1;

  while(k>=0) {
    if( IsLineFull(k) ) {
      RemoveLine(k);
      nbRemoved++;
    } else {
      k--;
    }
  }

  return nbRemoved;

}

//-----------------------------------------------------------------------------

void Pit::RemoveLine(int idx) {

  for(int k=idx;k>0;k--) {
    for(int i=0;i<width;i++)
      for(int j=0;j<height;j++)
        SetValue(i,j,k, GetValue(i,j,k-1) );
  }

  // Clear last line
  for(int i=0;i<width;i++)
    for(int j=0;j<height;j++)
      SetValue(i,j,0,0);

}

//-----------------------------------------------------------------------------

HRESULT Pit::Create(LPDIRECT3DDEVICE8 pd3dDevice,int style) {

  HRESULT hr;
  float startX;
  float startY;

  // Check dimension
  if( width*height*depth == 0 ) {
    // Invalid dimension
    // Does not create but return OK
    return S_OK;
  }

  this->style = style;

  // Init global variable

  if( width>height ) {
    cubeSide = 1.0f / (float)width;
    startX = -0.5f;
    startY = -0.5f + ( (float)(width-height)*cubeSide/2.0f );
  } else {
    cubeSide = 1.0f / (float)height;
    startX = -0.5f + ( (float)(height-width)*cubeSide/2.0f );
    startY = -0.5f;
  }

  fWidth  = (float)width  * cubeSide;
  fHeight = (float)height * cubeSide;
  fDepth  = (float)depth  * cubeSide;

  origin.x = startX;
  origin.y = startY;
  origin.z = STARTZ;

  // Init material
  memset (&blackMaterial, 0, sizeof (D3DMATERIAL8));

  memset (&gridMaterial, 0, sizeof (D3DMATERIAL8));
	gridMaterial.Diffuse.r = 1.0f;
	gridMaterial.Diffuse.g = 1.0f;
	gridMaterial.Diffuse.b = 1.0f;
	gridMaterial.Ambient.r = 0.0f;
	gridMaterial.Ambient.g = 0.6f;
	gridMaterial.Ambient.b = 0.0f;
  memset (&darkMaterial, 0, sizeof (D3DMATERIAL8));
	darkMaterial.Diffuse.r = 0.1f;
	darkMaterial.Diffuse.g = 0.1f;
	darkMaterial.Diffuse.b = 0.1f;
	darkMaterial.Ambient.r = 0.1f;
	darkMaterial.Ambient.g = 0.1f;
	darkMaterial.Ambient.b = 0.1f;
  memset (&backMaterial, 0, sizeof (D3DMATERIAL8));
	backMaterial.Diffuse.r = 0.05f;
	backMaterial.Diffuse.g = 0.05f;
	backMaterial.Diffuse.b = 0.1f;
	backMaterial.Ambient.r = 0.05f;
	backMaterial.Ambient.g = 0.05f;
	backMaterial.Ambient.b = 0.1f;
  memset (&backTexMaterial, 0, sizeof (D3DMATERIAL8));
	backTexMaterial.Diffuse.r = 0.35f;
	backTexMaterial.Diffuse.g = 0.35f;
	backTexMaterial.Diffuse.b = 0.35f;
	backTexMaterial.Ambient.r = 0.35f;
	backTexMaterial.Ambient.g = 0.35f;
	backTexMaterial.Ambient.b = 0.35f;

  // Init 3D objects
  hr = CreateGrid(pd3dDevice);
  if(FAILED(hr)) return hr;

  hr = CreateBack(pd3dDevice);
  if(FAILED(hr)) return hr;

  hr = CreateFillingCube(pd3dDevice);
  if(FAILED(hr)) return hr;

  hr = CreatePitLevel(pd3dDevice);
  if(FAILED(hr)) return hr;

  inited = TRUE;
  return S_OK;

}

//-----------------------------------------------------------------------------

void Pit::InitOrderMatrix() {

  double *dist;
  double xOrg = (double)width/2.0;
  double yOrg = (double)height/2.0;
  double zOrg = 0.0;

  orderMatrix = (BLOCKITEM *)malloc( sizeof(BLOCKITEM)*mSize );
  dist = (double *)malloc( sizeof(double)*mSize );

  // Init distance
  int l = 0;
  for(int k=0;k<depth;k++) {
    for(int j=0;j<height;j++) {
      for(int i=0;i<width;i++) {
        double xc = double(2*i+1)/2.0;
        double yc = double(2*j+1)/2.0;
        double zc = double(2*k+1)/2.0;
        dist[l] = (xc-xOrg)*(xc-xOrg) + 
                  (yc-yOrg)*(yc-yOrg) +
                  (zc-zOrg)*(zc-zOrg);
        orderMatrix[l].x = i;
        orderMatrix[l].y = j;
        orderMatrix[l].z = k;
        l++;
      }
    }
  }

  // Sort
  BOOL end = FALSE;
  int i=0;
  int j=mSize-1;

  while(!end) {
    end = TRUE;
    for(i=0;i<j;i++) {
      if( dist[i]<dist[i+1] ) {

        // Swap dist
        double tmp = dist[i];
        dist[i] = dist[i+1];
        dist[i+1] = tmp;
        // Swap orderMatrix
        BLOCKITEM tmp2 = orderMatrix[i];
        orderMatrix[i] = orderMatrix[i+1];
        orderMatrix[i+1] = tmp2;

        end = FALSE;
      }
    }
    j--;
  }

  free(dist);

}

//-----------------------------------------------------------------------------

HRESULT Pit::CreateBack(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;
  WORD *pIndices;
  FACEVERTEX *bVertices;
  VERTEX *pVertices;
  TFACEVERTEX *tVertices;
  int l;

  switch(style) {

  // ----- CLASSIC Style --------------------------------------------------------------
  case STYLE_CLASSIC:

  hr = pd3dDevice->CreateVertexBuffer(20 * sizeof(FACEVERTEX), 
            D3DUSAGE_DYNAMIC, FACEVERTEX_FORMAT, D3DPOOL_DEFAULT, &backVbf);
  if(FAILED(hr)) return hr;

  hr = backVbf->Lock(0, 20 * sizeof(FACEVERTEX), (BYTE **) &bVertices, 0);
  if(FAILED(hr)) return hr;

  hr = pd3dDevice->CreateIndexBuffer(30 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &backIdx);
  if(FAILED(hr)) return hr;

  hr = backIdx->Lock(0, 30 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l=0;

  // Face 1
  bVertices[0] = v( origin.x , origin.y + fHeight , origin.z          , 1,0,0 );
  bVertices[1] = v( origin.x , origin.y + fHeight , origin.z + fDepth , 1,0,0 );
  bVertices[2] = v( origin.x , origin.y           , origin.z + fDepth , 1,0,0 );
  bVertices[3] = v( origin.x , origin.y           , origin.z          , 1,0,0 );

  pIndices[l++] =  0;  pIndices[l++] =  1;  pIndices[l++] =  2;
  pIndices[l++] =  0;  pIndices[l++] =  2;  pIndices[l++] =  3;

  // Face 2
  bVertices[4] = v( origin.x          , origin.y + fHeight , origin.z          , 0,-1,0 );
  bVertices[5] = v( origin.x + fWidth , origin.y + fHeight , origin.z          , 0,-1,0 );
  bVertices[6] = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , 0,-1,0 );
  bVertices[7] = v( origin.x          , origin.y + fHeight , origin.z + fDepth , 0,-1,0 );

  pIndices[l++] =  4;  pIndices[l++] =  5;  pIndices[l++] =  6;
  pIndices[l++] =  4;  pIndices[l++] =  6;  pIndices[l++] =  7;

  // Face 3
  bVertices[8]  = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , -1,0,0 );
  bVertices[9]  = v( origin.x + fWidth , origin.y + fHeight , origin.z          , -1,0,0 );
  bVertices[10] = v( origin.x + fWidth , origin.y           , origin.z          , -1,0,0 );
  bVertices[11] = v( origin.x + fWidth , origin.y           , origin.z + fDepth , -1,0,0 );

  pIndices[l++] =  8;  pIndices[l++] =  9;  pIndices[l++] = 10;
  pIndices[l++] =  8;  pIndices[l++] = 10;  pIndices[l++] = 11;

  // Face 4
  bVertices[12]  = v( origin.x + fWidth , origin.y , origin.z          , 0,1,0 );
  bVertices[13]  = v( origin.x          , origin.y , origin.z          , 0,1,0 );
  bVertices[14]  = v( origin.x          , origin.y , origin.z + fDepth , 0,1,0 );
  bVertices[15]  = v( origin.x + fWidth , origin.y , origin.z + fDepth , 0,1,0 );

  pIndices[l++] =  12;  pIndices[l++] = 13;  pIndices[l++] = 14;
  pIndices[l++] =  12;  pIndices[l++] = 14;  pIndices[l++] = 15;

  // Face 5
  bVertices[16]  = v( origin.x          , origin.y           , origin.z + fDepth , 0,0,-1 );
  bVertices[17]  = v( origin.x + fWidth , origin.y           , origin.z + fDepth , 0,0,-1 );
  bVertices[18]  = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , 0,0,-1 );
  bVertices[19]  = v( origin.x          , origin.y + fHeight , origin.z + fDepth , 0,0,-1 );

  pIndices[l++] =  16;  pIndices[l++] = 19;  pIndices[l++] = 18;
  pIndices[l++] =  16;  pIndices[l++] = 18;  pIndices[l++] = 17;

  backVbf->Unlock();
  backIdx->Unlock();
  break;

  // ----- MARBLE Style --------------------------------------------------------------
  case STYLE_MARBLE: {

  hr = pd3dDevice->CreateVertexBuffer(20 * sizeof(TFACEVERTEX), 
            D3DUSAGE_DYNAMIC, TFACEVERTEX_FORMAT, D3DPOOL_DEFAULT, &backVbf);
  if(FAILED(hr)) return hr;

  hr = backVbf->Lock(0, 20 * sizeof(TFACEVERTEX), (BYTE **) &tVertices, 0);
  if(FAILED(hr)) return hr;

  hr = pd3dDevice->CreateIndexBuffer(30 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &backIdx);
  if(FAILED(hr)) return hr;

  hr = backIdx->Lock(0, 30 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l=0;
  float mx = (float)width /4.0f;
  float my = (float)height/4.0f;
  float mz = (float)depth /4.0f;

  // Face 1
  tVertices[0] = v( origin.x , origin.y + fHeight , origin.z          , 1,0,0 , mz   , 0.0f );
  tVertices[1] = v( origin.x , origin.y + fHeight , origin.z + fDepth , 1,0,0 , 0.0f , 0.0f );
  tVertices[2] = v( origin.x , origin.y           , origin.z + fDepth , 1,0,0 , 0.0f , my   );
  tVertices[3] = v( origin.x , origin.y           , origin.z          , 1,0,0 , mz   , my   );

  pIndices[l++] =  0;  pIndices[l++] =  1;  pIndices[l++] =  2;
  pIndices[l++] =  0;  pIndices[l++] =  2;  pIndices[l++] =  3;

  // Face 2
  tVertices[4] = v( origin.x          , origin.y + fHeight , origin.z          , 0,-1,0 , mz   , mx    );
  tVertices[5] = v( origin.x + fWidth , origin.y + fHeight , origin.z          , 0,-1,0 , mz   , 0.0f  );
  tVertices[6] = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , 0,-1,0 , 0.0f , 0.0f  );
  tVertices[7] = v( origin.x          , origin.y + fHeight , origin.z + fDepth , 0,-1,0 , 0.0f , mx    );

  pIndices[l++] =  4;  pIndices[l++] =  5;  pIndices[l++] =  6;
  pIndices[l++] =  4;  pIndices[l++] =  6;  pIndices[l++] =  7;

  // Face 3
  tVertices[8]  = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , -1,0,0 , 0.0f , my   );
  tVertices[9]  = v( origin.x + fWidth , origin.y + fHeight , origin.z          , -1,0,0 , mz   , my   );
  tVertices[10] = v( origin.x + fWidth , origin.y           , origin.z          , -1,0,0 , mz   , 0.0f );
  tVertices[11] = v( origin.x + fWidth , origin.y           , origin.z + fDepth , -1,0,0 , 0.0f , 0.0f );

  pIndices[l++] =  8;  pIndices[l++] =  9;  pIndices[l++] = 10;
  pIndices[l++] =  8;  pIndices[l++] = 10;  pIndices[l++] = 11;

  // Face 4
  tVertices[12]  = v( origin.x + fWidth , origin.y , origin.z          , 0,1,0, mz   , mx   );
  tVertices[13]  = v( origin.x          , origin.y , origin.z          , 0,1,0, mz   , 0.0f );
  tVertices[14]  = v( origin.x          , origin.y , origin.z + fDepth , 0,1,0, 0.0f , 0.0f );
  tVertices[15]  = v( origin.x + fWidth , origin.y , origin.z + fDepth , 0,1,0, 0.0f , mx   );

  pIndices[l++] =  12;  pIndices[l++] = 13;  pIndices[l++] = 14;
  pIndices[l++] =  12;  pIndices[l++] = 14;  pIndices[l++] = 15;

  // Face 5
  tVertices[16]  = v( origin.x          , origin.y           , origin.z + fDepth , 0,0,-1, mx   , 0.0f );
  tVertices[17]  = v( origin.x + fWidth , origin.y           , origin.z + fDepth , 0,0,-1, 0.0f , 0.0f );
  tVertices[18]  = v( origin.x + fWidth , origin.y + fHeight , origin.z + fDepth , 0,0,-1, 0.0f , my   );
  tVertices[19]  = v( origin.x          , origin.y + fHeight , origin.z + fDepth , 0,0,-1, mx   , my   );

  pIndices[l++] =  16;  pIndices[l++] = 19;  pIndices[l++] = 18;
  pIndices[l++] =  16;  pIndices[l++] = 18;  pIndices[l++] = 17;

  backVbf->Unlock();
  backIdx->Unlock();

  // Marble texture
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  hr = CreateTexture(pd3dDevice,512,512,desc.Format,"images\\marbleg.png",&backTexture);
  if(FAILED(hr)) return hr;

  // Marble block texture
  hr = CreateTexture(pd3dDevice,256,256,desc.Format,"images\\marble.png",&cubeTexture);
  if(FAILED(hr)) return hr;

  pBackBuffer->Release();
  }
  break;

  // ----- ARCADE Style --------------------------------------------------------------
  case STYLE_ARCADE: {

  // Crystal block texture
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  hr = CreateTexture(pd3dDevice,256,256,desc.Format,"images\\star_crystal_grid.png",&cubeTexture);
  if(FAILED(hr)) return hr;
  pBackBuffer->Release();

  }

  break;

  } // End switch(style)

  // ------------------------------------------------------------------------
  // Side (Black polygon around the pit, for faster repaint)
  // ------------------------------------------------------------------------

  hr = pd3dDevice->CreateVertexBuffer(8 * sizeof(VERTEX), 
            D3DUSAGE_DYNAMIC, VERTEX_FORMAT, D3DPOOL_DEFAULT, &sideVbf);
  if(FAILED(hr)) return hr;

  hr = sideVbf->Lock(0, 8 * sizeof(VERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  hr = pd3dDevice->CreateIndexBuffer(24 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &sideIdx);
  if(FAILED(hr)) return hr;

  hr = sideIdx->Lock(0, 24 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l=0;

  pVertices[0]  = v( origin.x          , origin.y           , origin.z );
  pVertices[1]  = v( origin.x + fWidth , origin.y           , origin.z );
  pVertices[2]  = v( origin.x + fWidth , origin.y + fHeight , origin.z );
  pVertices[3]  = v( origin.x          , origin.y + fHeight , origin.z );

  pVertices[4]  = v( origin.x          - cubeSide , origin.y           - cubeSide , origin.z );
  pVertices[5]  = v( origin.x + fWidth + cubeSide , origin.y           - cubeSide , origin.z );
  pVertices[6]  = v( origin.x + fWidth + cubeSide , origin.y + fHeight + cubeSide , origin.z );
  pVertices[7]  = v( origin.x          - cubeSide , origin.y + fHeight + cubeSide , origin.z );

  pIndices[l++] =  2;  pIndices[l++] = 7;  pIndices[l++] = 6;
  pIndices[l++] =  2;  pIndices[l++] = 3;  pIndices[l++] = 7;
  pIndices[l++] =  3;  pIndices[l++] = 4;  pIndices[l++] = 7;
  pIndices[l++] =  3;  pIndices[l++] = 0;  pIndices[l++] = 4;
  pIndices[l++] =  0;  pIndices[l++] = 5;  pIndices[l++] = 4;
  pIndices[l++] =  0;  pIndices[l++] = 1;  pIndices[l++] = 5;
  pIndices[l++] =  5;  pIndices[l++] = 1;  pIndices[l++] = 2;
  pIndices[l++] =  5;  pIndices[l++] = 2;  pIndices[l++] = 6;

  sideVbf->Unlock();
  sideIdx->Unlock();

  return S_OK;

}

//-----------------------------------------------------------------------------

HRESULT Pit::CreateGrid(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;
  VERTEX *pVertices;
  WORD *pIndices;

  int nbVPlane = ( (width+1)*2 + (height-1)*2 );
  nbGridVertex = nbVPlane * 2 + (depth+1)*4;

  // Vertex
  hr = pd3dDevice->CreateVertexBuffer(nbGridVertex * sizeof(VERTEX), 
            D3DUSAGE_DYNAMIC, VERTEX_FORMAT, D3DPOOL_DEFAULT, &gridVbf);
  if(FAILED(hr)) return hr;

  hr = gridVbf->Lock(0, nbGridVertex * sizeof(VERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  // Pit
  int l = 0;

  for(int i=0;i<=width;i++) {
    pVertices[l++] = v( origin.x + (i*cubeSide), -origin.y , origin.z );
    pVertices[l++] = v( origin.x + (i*cubeSide),  origin.y , origin.z );
    pVertices[l++] = v( origin.x + (i*cubeSide), -origin.y , origin.z + fDepth );
    pVertices[l++] = v( origin.x + (i*cubeSide),  origin.y , origin.z + fDepth );
  }

  for(int i=1;i<height;i++) {
    pVertices[l++] = v(  origin.x , origin.y + (i*cubeSide), origin.z );
    pVertices[l++] = v( -origin.x , origin.y + (i*cubeSide), origin.z );
    pVertices[l++] = v(  origin.x , origin.y + (i*cubeSide), origin.z + fDepth );
    pVertices[l++] = v( -origin.x , origin.y + (i*cubeSide), origin.z + fDepth );
  }

  for(int i=0;i<=depth;i++) {
    pVertices[l++] = v(  origin.x , -origin.y , origin.z + (i*cubeSide) );
    pVertices[l++] = v( -origin.x , -origin.y , origin.z + (i*cubeSide) );
    pVertices[l++] = v( -origin.x ,  origin.y , origin.z + (i*cubeSide) );
    pVertices[l++] = v(  origin.x ,  origin.y , origin.z + (i*cubeSide) );
  }

  gridVbf->Unlock();

  nbGridIndex = (depth+1) * 8 + nbVPlane * 2 + (width-1+height-1) * 2;

  hr = pd3dDevice->CreateIndexBuffer(nbGridIndex * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &gridIdx);
  if(FAILED(hr)) return hr;

  hr = gridIdx->Lock(0, nbGridIndex * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l = 0;
  for(int i=0;i<=width;i++) {
    pIndices[l++] = 4*i;
    pIndices[l++] = 4*i+2;
    pIndices[l++] = 4*i+1;
    pIndices[l++] = 4*i+3;
  }

  for(int i=0;i<height-1;i++) {
    pIndices[l++] = 4*(width+1) + 4*i;
    pIndices[l++] = 4*(width+1) + 4*i+2;
    pIndices[l++] = 4*(width+1) + 4*i+1;
    pIndices[l++] = 4*(width+1) + 4*i+3;
  }

  for(int i=0;i<=depth;i++) {
    pIndices[l++] = 2*nbVPlane + 4*i+0;
    pIndices[l++] = 2*nbVPlane + 4*i+1;
    pIndices[l++] = 2*nbVPlane + 4*i+1;
    pIndices[l++] = 2*nbVPlane + 4*i+2;
    pIndices[l++] = 2*nbVPlane + 4*i+2;
    pIndices[l++] = 2*nbVPlane + 4*i+3;
    pIndices[l++] = 2*nbVPlane + 4*i+3;
    pIndices[l++] = 2*nbVPlane + 4*i+0;
  }

  for(int i=1;i<width;i++) {
    pIndices[l++] = 4*i+2;
    pIndices[l++] = 4*i+3;
  }

  for(int i=0;i<height-1;i++) {
    pIndices[l++] = 4*(width+1) + 4*i+2;
    pIndices[l++] = 4*(width+1) + 4*i+3;
  }

  gridIdx->Unlock();

  return S_OK;

}

//-----------------------------------------------------------------------------

HRESULT Pit::CreatePitLevel(LPDIRECT3DDEVICE8 pd3dDevice) {

  // Get back buffer format
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  pBackBuffer->Release();

  pitLevelX = fround( (float)desc.Width  * 0.0176f );
  pitLevelY = fround( (float)desc.Height * 0.1510f );
  pitLevelW = fround( (float)desc.Width  * 0.0498f );
  pitLevelH = fround( (float)desc.Height * 0.8255f );
  pitLevelF = desc.Format;

  return S_OK;

}

//-----------------------------------------------------------------------------

void Pit::ChooseMapping(int i,float *sx,float *sy,float *ex,float *ey) {

  float c[] = { 0.0f , 0.25f , 0.5f , 0.75f };
  float _1px = 1.0f / 256.0f;

  *sx = c[(i/4)%4];
  *sy = c[i%4];

  *ex = *sx + 0.25f;
  *ey = *sy + 0.25f;

  *sx += _1px;
  *sy += _1px;
  *ex -= _1px;
  *ey -= _1px;

}

//-----------------------------------------------------------------------------
   
HRESULT Pit::CreateTexturedFillingCube(LPDIRECT3DDEVICE8 pd3dDevice,int i,LPDIRECT3DVERTEXBUFFER8 *vBuffer) {

  TFACEVERTEX *tVertices;
  HRESULT hr;

  hr = pd3dDevice->CreateVertexBuffer(24 * sizeof(TFACEVERTEX), 
            D3DUSAGE_DYNAMIC, TFACEVERTEX_FORMAT, D3DPOOL_DEFAULT, vBuffer);
  if(FAILED(hr)) return hr;

  hr = (*vBuffer)->Lock(0, 24 * sizeof(TFACEVERTEX), (BYTE **) &tVertices, 0);
  if(FAILED(hr)) return hr;

  float sx;
  float sy;
  float ex;
  float ey;

  // Face 1
  ChooseMapping(i,&sx,&sy,&ex,&ey);

  tVertices[0] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 , sx, sy);

  tVertices[1] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 , ex, sy);

  tVertices[2] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 , ex, ey);

  tVertices[3] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 , sx, ey);

  // Face 2
  ChooseMapping(i+NBTCUBE,&sx,&sy,&ex,&ey);

  tVertices[4] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    1,0,0 , sx, sy);

  tVertices[5] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    1,0,0 , ex, sy);

  tVertices[6] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    1,0,0 , ex, ey);

  tVertices[7] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    1,0,0 , sx, ey);

  // Face 3
  ChooseMapping(i+2*NBTCUBE,&sx,&sy,&ex,&ey);

  tVertices[8] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 , sx, sy);

  tVertices[9] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 , ex, sy);

  tVertices[10] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 , ex, ey);

  tVertices[11] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 , sx, ey);

  // Face 4
  ChooseMapping(i+3*NBTCUBE,&sx,&sy,&ex,&ey);

  tVertices[12] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     -1,0,0 , sx, sy);

  tVertices[13] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     -1,0,0 , ex, sy);

  tVertices[14] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     -1,0,0 , ex, ey);

  tVertices[15] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     -1,0,0 , sx, ey);

  // Face 5
  ChooseMapping(i+4*NBTCUBE,&sx,&sy,&ex,&ey);

  tVertices[16] = v( 1 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,-1,0 , sx, sy);

  tVertices[17] = v( 1 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,-1,0 , ex, sy);

  tVertices[18] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,-1,0 , ex, ey);

  tVertices[19] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,-1,0 , sx, ey);

  // Face 6
  ChooseMapping(i+5*NBTCUBE,&sx,&sy,&ex,&ey);

  tVertices[20] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,1,0 , sx, sy);

  tVertices[21] = v( 1 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,1,0 , ex, sy);

  tVertices[22] = v( 1 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,1,0 , ex, ey);

  tVertices[23] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,1,0 , sx, ey);

  (*vBuffer)->Unlock();

  return S_OK;

}

//-----------------------------------------------------------------------------

HRESULT Pit::CreateFillingCube(LPDIRECT3DDEVICE8 pd3dDevice) {

  FACEVERTEX *pVertices;
  VERTEX *lVertices;
  WORD *pIndices;
  HRESULT hr;
  int l = 0;

  switch(style) {

  // ----- CLASSIC Style --------------------------------------------------------------
  case STYLE_CLASSIC:

  hr = pd3dDevice->CreateVertexBuffer(24 * sizeof(FACEVERTEX), 
            D3DUSAGE_DYNAMIC, FACEVERTEX_FORMAT, D3DPOOL_DEFAULT, &cubeVbf);
  if(FAILED(hr)) return hr;

  hr = cubeVbf->Lock(0, 24 * sizeof(FACEVERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  // Face 1

  pVertices[0] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 );

  pVertices[1] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 );

  pVertices[2] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 );

  pVertices[3] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    0,0,-1 );

  // Face 2

  pVertices[4] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    1,0,0 );

  pVertices[5] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    1,0,0 );

  pVertices[6] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    1,0,0 );

  pVertices[7] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z,
                    1,0,0 );

  // Face 3

  pVertices[8] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 );

  pVertices[9] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 );

  pVertices[10] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 );

  pVertices[11] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z,
                    0,0,1 );

  // Face 4

  pVertices[12] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     -1,0,0 );

  pVertices[13] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     -1,0,0 );

  pVertices[14] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     -1,0,0 );

  pVertices[15] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     -1,0,0 );

  // Face 5

  pVertices[16] = v( 1 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,-1,0 );

  pVertices[17] = v( 1 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,-1,0 );

  pVertices[18] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,-1,0 );

  pVertices[19] = v( 0 * cubeSide + origin.x,
                     0 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,-1,0 );

  // Face 6

  pVertices[20] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,1,0 );

  pVertices[21] = v( 1 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     1 * cubeSide + origin.z,
                     0,1,0 );

  pVertices[22] = v( 1 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,1,0 );

  pVertices[23] = v( 0 * cubeSide + origin.x,
                     1 * cubeSide + origin.y,
                     0 * cubeSide + origin.z,
                     0,1,0 );

  cubeVbf->Unlock();
  break;

  // ----- MARBLE/ARCADE Style --------------------------------------------------------------
  case STYLE_MARBLE:
  case STYLE_ARCADE:

  for(int i=0;i<NBTCUBE;i++) {

    hr = CreateTexturedFillingCube(pd3dDevice,i,cubeTVbf + i);
    if(FAILED(hr)) return hr;

  }
  break;

  } // End switch(style)

  hr = pd3dDevice->CreateIndexBuffer(36 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &cubeIdx);
  if(FAILED(hr)) return hr;

  hr = cubeIdx->Lock(0, 36 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l = 0;
  pIndices[l++] =  0;  pIndices[l++] =  1;  pIndices[l++] =  2;
  pIndices[l++] =  0;  pIndices[l++] =  2;  pIndices[l++] =  3;

  pIndices[l++] =  4;  pIndices[l++] =  5;  pIndices[l++] =  6;
  pIndices[l++] =  4;  pIndices[l++] =  6;  pIndices[l++] =  7;

  pIndices[l++] =  8;  pIndices[l++] =  9;  pIndices[l++] = 10;
  pIndices[l++] =  8;  pIndices[l++] = 10;  pIndices[l++] = 11;

  pIndices[l++] =  12;  pIndices[l++] = 13;  pIndices[l++] = 14;
  pIndices[l++] =  12;  pIndices[l++] = 14;  pIndices[l++] = 15;

  pIndices[l++] =  16;  pIndices[l++] = 17;  pIndices[l++] = 18;
  pIndices[l++] =  16;  pIndices[l++] = 18;  pIndices[l++] = 19;

  pIndices[l++] =  20;  pIndices[l++] = 21;  pIndices[l++] = 22;
  pIndices[l++] =  20;  pIndices[l++] = 22;  pIndices[l++] = 23;


  cubeIdx->Unlock();

  // --------------------------------------------------------------------------

  // Cube contours

  hr = pd3dDevice->CreateVertexBuffer(8 * sizeof(VERTEX), 
            D3DUSAGE_DYNAMIC, VERTEX_FORMAT, D3DPOOL_DEFAULT, &lcubeVbf);
  if(FAILED(hr)) return hr;

  hr = lcubeVbf->Lock(0, 8 * sizeof(VERTEX), (BYTE **) &lVertices, 0);
  if(FAILED(hr)) return hr;

  lVertices[0] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z);

  lVertices[1] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    0 * cubeSide + origin.z);

  lVertices[2] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z);

  lVertices[3] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    0 * cubeSide + origin.z);

  lVertices[4] = v( 0 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z);

  lVertices[5] = v( 1 * cubeSide + origin.x,
                    1 * cubeSide + origin.y,
                    1 * cubeSide + origin.z);

  lVertices[6] = v( 1 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z);

  lVertices[7] = v( 0 * cubeSide + origin.x,
                    0 * cubeSide + origin.y,
                    1 * cubeSide + origin.z);

  lcubeVbf->Unlock();

  // ------------------------------------------
  for(int i=0;i<12;i++) {

    hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &(lcubeIdx[i]));
    if(FAILED(hr)) return hr;

    hr = lcubeIdx[i]->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
    if(FAILED(hr)) return hr;

    switch(i) {
      case 0:
        pIndices[0] = 0; pIndices[1] = 1;
        break;
      case 1:
        pIndices[0] = 1; pIndices[1] = 2;
        break;
      case 2:
        pIndices[0] = 2; pIndices[1] = 3;
        break;
      case 3:
        pIndices[0] = 3; pIndices[1] = 0;
        break;
      case 4:
        pIndices[0] = 0; pIndices[1] = 4;
        break;
      case 5:
        pIndices[0] = 1; pIndices[1] = 5;
        break;
      case 6:
        pIndices[0] = 2; pIndices[1] = 6;
        break;
      case 7:
        pIndices[0] = 3; pIndices[1] = 7;
        break;
      case 8:
        pIndices[0] = 4; pIndices[1] = 5;
        break;
      case 9:
        pIndices[0] = 5; pIndices[1] = 6;
        break;
      case 10:
        pIndices[0] = 7; pIndices[1] = 6;
        break;
      case 11:
        pIndices[0] = 7; pIndices[1] = 4;
        break;
    }

    lcubeIdx[i]->Unlock();

  }

  return S_OK;

}

//-----------------------------------------------------------------------------

D3DMATERIAL8 *Pit::GetMaterial(int level) {

  static D3DMATERIAL8 ret;
  memset (&ret, 0, sizeof (D3DMATERIAL8));

  switch((depth-level-1) % 7) {

    case 0:
	    ret.Diffuse.r = 0.0f;
	    ret.Diffuse.g = 0.0f;
	    ret.Diffuse.b = 1.0f;
      break;

    case 1:
	    ret.Diffuse.r = 0.0f;
	    ret.Diffuse.g = 1.0f;
	    ret.Diffuse.b = 0.0f;
      break;

    case 2:
	    ret.Diffuse.r = 0.0f;
	    ret.Diffuse.g = 0.9f;
	    ret.Diffuse.b = 0.9f;
      break;

    case 3:
	    ret.Diffuse.r = 1.0f;
	    ret.Diffuse.g = 0.0f;
	    ret.Diffuse.b = 0.0f;
      break;

    case 4:
	    ret.Diffuse.r = 1.0f;
	    ret.Diffuse.g = 0.1f;
	    ret.Diffuse.b = 0.8f;
      break;

    case 5:
	    ret.Diffuse.r = 0.9f;
	    ret.Diffuse.g = 0.6f;
	    ret.Diffuse.b = 0.0f;
      break;

    case 6:
	    ret.Diffuse.r = 0.85f;
	    ret.Diffuse.g = 0.85f;
	    ret.Diffuse.b = 0.85f;
      break;

  }

  ret.Ambient = ret.Diffuse;

  ret.Diffuse.r *= 0.3f;
  ret.Diffuse.g *= 0.3f;
  ret.Diffuse.b *= 0.3f;

  ret.Ambient.r *= 0.7f;
  ret.Ambient.g *= 0.7f;
  ret.Ambient.b *= 0.7f;

  return &ret;

}
//-----------------------------------------------------------------------------

BOOL Pit::IsVisible(int x,int y,int z) {
  
  if( z==0 ) return TRUE;

  return ( GetValue(x+1,y,z)==0 ||
           GetValue(x-1,y,z)==0 ||
           GetValue(x,y+1,z)==0 ||
           GetValue(x,y-1,z)==0 ||
           GetValue(x,y,z-1)==0 );

}

//-----------------------------------------------------------------------------

BOOL Pit::IsVisible2(int x,int y,int z) {
  
  if( z==0 ) return TRUE;

  return ( GetValue2(x+1,y,z)==0 ||
           GetValue2(x-1,y,z)==0 ||
           GetValue2(x,y+1,z)==0 ||
           GetValue2(x,y-1,z)==0 ||
           GetValue2(x,y,z-1)==0 );

}

//-----------------------------------------------------------------------------

void Pit::Clear() {

  if( matrix )
    memset( matrix , 0 , mSize*sizeof(int) );
  tcubeIdx = 1;

}

//-----------------------------------------------------------------------------

int Pit::GetWidth() {
  return width;
}

int Pit::GetHeight() {
  return height;
}

int Pit::GetDepth() {
  return depth;
}

//-----------------------------------------------------------------------------

VERTEX Pit::GetOrigin() {

  return origin;

}

//-----------------------------------------------------------------------------

float Pit::GetCubeSide() {

  return cubeSide;

}

//-----------------------------------------------------------------------------

void Pit::GetOutOfBounds(int x,int y,int z,int *ox,int *oy,int *oz) {

  *ox = 0;
  *oy = 0;
  *oz = 0;

  if( x<0 )
    *ox = -x;

  if( x>=width )
    *ox = width - x - 1;

  if( y<0 )
    *oy = -y;

  if( y>=height )
    *oy = height - y - 1;

  if( z<0 )
    *oz = -z;

  if( z>=depth )
    *oz = depth - z - 1;

}

//-----------------------------------------------------------------------------

int Pit::GetValue(int x,int y,int z) {

  if( x<0 || x>=width || y<0 || y>=height || z<0 || z>=depth )
    return 1;
  else
    return matrix[x + y*width + z*width*height];

}

//-----------------------------------------------------------------------------

int Pit::GetValue2(int x,int y,int z) {

  if( x<0 || x>=width || y<0 || y>=height || z<0 || z>=depth )
    return 0;
  else
    return matrix[x + y*width + z*width*height];

}

//-----------------------------------------------------------------------------

BOOL Pit::IsLineFull(int z) {

  BOOL full = TRUE;
  for(int i=0;i<width && full;i++)
    for(int j=0;j<height && full;j++)
      full = full && (GetValue(i,j,z)>=1);
  return full;

}

//-----------------------------------------------------------------------------

BOOL Pit::IsLineEmpty(int z) {

  BOOL empty = TRUE;
  for(int i=0;i<width && empty;i++)
    for(int j=0;j<height && empty;j++)
      empty = empty && (GetValue(i,j,z)==0);
  return empty;

}

//-----------------------------------------------------------------------------

void Pit::SetValue(int x,int y,int z,int value) {

  if( x>=0 && x<width && y>=0 && y<height && z>=0 && z<depth ) {
    matrix[x + y*width + z*width*height] = value;
  }

}

//-----------------------------------------------------------------------------

void Pit::AddCube(int x,int y,int z) {

  tcubeIdx++;
  if(tcubeIdx>NBTCUBE) tcubeIdx=1;
  SetValue(x,y,z,tcubeIdx);

}

//-----------------------------------------------------------------------------

void Pit::RenderEdge(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y,int z,int edge) {

  BOOL isGreen=FALSE;

  if( style==STYLE_CLASSIC ) {

  switch(edge) {
    case 0:
      isGreen = (y==height-1);
      break;
    case 1:
      isGreen = (x==width-1);
      break;
    case 2:
      isGreen = (y==0);
      break;
    case 3:
      isGreen = (x==0);
      break;
    case 4:
      isGreen = (x==0) || (y==height-1);
      break;
    case 5:
      isGreen = (x==width-1) || (y==height-1);
      break;
    case 6:
      isGreen = (x==width-1) || (y==0);
      break;
    case 7:
      isGreen = (x==0) || (y==0);
      break;
    case 8:
    case 9:
    case 10:
    case 11:
      isGreen = (z==depth-1);
      break;
  }

  }

  if( isGreen )
    pd3dDevice->SetMaterial( &gridMaterial );
  else
    pd3dDevice->SetMaterial( &blackMaterial );

  pd3dDevice->SetIndices( lcubeIdx[edge], 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );

}

//-----------------------------------------------------------------------------

void Pit::RenderCube(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y,int z) {

  D3DXMATRIX matPos;

  D3DXMatrixTranslation(&matPos,x*cubeSide,y*cubeSide,z*cubeSide);
  pd3dDevice->SetTransform( D3DTS_WORLD , &matPos);

  int mX = width/2;
  int mY = height/2;

  switch(style) {

  case STYLE_CLASSIC:

  // Draw the cube
  pd3dDevice->SetMaterial( GetMaterial(z) );
  pd3dDevice->SetVertexShader( FACEVERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, cubeVbf , sizeof(FACEVERTEX) );
  pd3dDevice->SetIndices( cubeIdx, 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 24, 0, 12 );

  // Draw the contour
  pd3dDevice->SetVertexShader( VERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, lcubeVbf , sizeof(VERTEX) );

  // Up
  RenderEdge(pd3dDevice,x,y,z,0);
  RenderEdge(pd3dDevice,x,y,z,1);
  RenderEdge(pd3dDevice,x,y,z,2);
  RenderEdge(pd3dDevice,x,y,z,3);

  // East
  if( x<mX ) {
    RenderEdge(pd3dDevice,x,y,z,5);
    RenderEdge(pd3dDevice,x,y,z,9);
    RenderEdge(pd3dDevice,x,y,z,6);
  }

  // West
  if( x>mX ) {
    RenderEdge(pd3dDevice,x,y,z,4);
    RenderEdge(pd3dDevice,x,y,z,11);
    RenderEdge(pd3dDevice,x,y,z,7);
  }

  // North
  if( y<mY ) {
    RenderEdge(pd3dDevice,x,y,z,4);
    RenderEdge(pd3dDevice,x,y,z,8);
    RenderEdge(pd3dDevice,x,y,z,5);
  }

  // South
  if( y>mY ) {
    RenderEdge(pd3dDevice,x,y,z,7);
    RenderEdge(pd3dDevice,x,y,z,10);
    RenderEdge(pd3dDevice,x,y,z,6);
  }
  break;

  case STYLE_MARBLE:
  case STYLE_ARCADE:

  // Enable texture
  pd3dDevice->SetTexture( 0, cubeTexture );
  pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
  pd3dDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );

  // Draw the cube
  pd3dDevice->SetMaterial( GetMaterial(z) );
  pd3dDevice->SetVertexShader( TFACEVERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, cubeTVbf[GetValue(x,y,z)-1] , sizeof(TFACEVERTEX) );
  pd3dDevice->SetIndices( cubeIdx, 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 24, 0, 12 );
    
  // Disable texture
  pd3dDevice->SetTexture( 0, NULL );
  pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_DISABLE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
  break;
  
  }  // End switch(style)

  return;

}

//-----------------------------------------------------------------------------

void Pit::RenderLevel(LPDIRECT3DDEVICE8 pd3dDevice,LPDIRECT3DSURFACE8 backBuffer) {

   if( !inited ) return;

   if( pitLevelW < 8 ) {
     // TODO small pit level
     return;
   }

   // Draw the pit level
   DWORD green = 0x00A000;
   D3DLOCKED_RECT  d3dlr;
   HRESULT hr = backBuffer->LockRect( &d3dlr, NULL, 0 );
   if( FAILED( hr ) ) return;

   int cS = (pitLevelH-2) / 20;
   int sY = pitLevelH - (cS * depth) - 2;

   for(int j=0;j<depth;j++) {

     // Border

     SetPix(&d3dlr,pitLevelF,1+pitLevelX,sY+pitLevelY,green);
     SetPix(&d3dlr,pitLevelF,2+pitLevelX,sY+pitLevelY,green);
     SetPix(&d3dlr,pitLevelF,pitLevelW-3+pitLevelX,sY+pitLevelY,green);
     SetPix(&d3dlr,pitLevelF,pitLevelW-2+pitLevelX,sY+pitLevelY,green);
     for(int i=sY;i<sY+cS;i++) {
       SetPix(&d3dlr,pitLevelF,0+pitLevelX,i+pitLevelY,green);
       SetPix(&d3dlr,pitLevelF,pitLevelW-1+pitLevelX,i+pitLevelY,green);
     }

     // Cube
     if( !IsLineEmpty(j) ) {

       D3DMATERIAL8 *m = GetMaterial(j);
       DWORD r = fround(m->Ambient.r * 255.0f);
       DWORD g = fround(m->Ambient.g * 255.0f);
       DWORD b = fround(m->Ambient.b * 255.0f);
       DWORD col = (r << 16) + (g << 8) + b;
       DrawPitLevelCubes(&d3dlr,pitLevelF,3+pitLevelX,sY+2+pitLevelY,pitLevelW-6,cS-3,col);

     }

     sY += cS;
   }

   for(int i=0;i<pitLevelW;i++) {
     SetPix(&d3dlr,pitLevelF,i+pitLevelX,pitLevelH-2+pitLevelY,green);
   }

   backBuffer->UnlockRect();

}

//-----------------------------------------------------------------------------

void Pit::DrawPitLevelCubes(D3DLOCKED_RECT *d3dlr,D3DFORMAT format,int x,int y,int w,int h,DWORD color) {

  DWORD white = 0x909090;

  // Cubes coordinates
  int f1 = fround( (float)h/4.0f );
  int f2 = fround( (float)h/2.0f );
  int wc = w/3;
  int sX = h/2;
  int eX = sX+2*wc;

  // ----- Filling

  for(int j=0;j<h;j++) {

    if( j>f2 ) eX--;

    for(int i=sX;i<eX;i++)
      SetPix(d3dlr,format,i+x,j+y,color);

    sX--;
    if( sX<0 ) sX=0;

  }

  // ----- Side
  sX = h/2;

  for(int j=0;j<h;j++) {
    
    if( j==0 || j==f1 || j==f2 || j==h-1 ) {

      // Plein green line
      for(int i=sX;i<=sX+2*wc;i++)
        SetPix(d3dlr,format,i+x,j+y,white);

    } else {

      // Dotted line      
      SetPix(d3dlr,format,sX+x,j+y,white);
      SetPix(d3dlr,format,sX+wc+x,j+y,white);
      SetPix(d3dlr,format,sX+2*wc+x,j+y,white);
    }

    // South east side
    if( sX>0 ) {
      SetPix(d3dlr,format,sX+x+2*wc,j+y+h/2,white);
    }
    // End verticals
    if( j>=0 && j<=h/2 ) {
      SetPix(d3dlr,format,h/2 + 2*wc + x,j+y,white);
    }
    if( j>=f1 && j<=f1+h/2 ) {
      SetPix(d3dlr,format,h/2 - f1 + 2*wc + x,j+y,white);
    }

    sX--;
    if( sX<0 ) sX=0;

  }

}

//-----------------------------------------------------------------------------

void Pit::Render(LPDIRECT3DDEVICE8 pd3dDevice,BOOL renderCube,BOOL zBuffer) {

  int nbVPlane;

  if( !inited ) return;

  // Disable texture, alpha
  pd3dDevice->SetTexture( 0, NULL );
  pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_DISABLE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
  pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
  pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );	  
  pd3dDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_POINT );
  pd3dDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_POINT );

  if( zBuffer )
    pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_TRUE);
  else
    pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);

  switch(style) {

  case STYLE_CLASSIC:

  // Draw side
  if( !zBuffer ) {
    pd3dDevice->SetMaterial (&blackMaterial);		
    pd3dDevice->SetVertexShader( VERTEX_FORMAT );
    pd3dDevice->SetIndices( sideIdx, 0 );
    pd3dDevice->SetStreamSource( 0, sideVbf , sizeof(VERTEX) );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 8, 0, 8 );

    // Draw the back
    pd3dDevice->SetMaterial (&backMaterial);		
    pd3dDevice->SetVertexShader( FACEVERTEX_FORMAT );
    pd3dDevice->SetIndices( backIdx, 0 );
    pd3dDevice->SetStreamSource( 0, backVbf , sizeof(FACEVERTEX) );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 20, 0, 10 );
  }

  // Draw the grid
  pd3dDevice->SetMaterial (&gridMaterial);		
  pd3dDevice->SetVertexShader( VERTEX_FORMAT );
  pd3dDevice->SetIndices( gridIdx, 0 );
  pd3dDevice->SetStreamSource( 0, gridVbf , sizeof(VERTEX) );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, nbGridVertex, 0, nbGridIndex/2 );
  break;

  case STYLE_MARBLE:

  // Draw side
  if( !zBuffer ) {
    pd3dDevice->SetMaterial (&blackMaterial);		
    pd3dDevice->SetVertexShader( VERTEX_FORMAT );
    pd3dDevice->SetIndices( sideIdx, 0 );
    pd3dDevice->SetStreamSource( 0, sideVbf , sizeof(VERTEX) );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 8, 0, 8 );
  }

  // Enable texture
  pd3dDevice->SetTexture( 0, backTexture );
  pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
  pd3dDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );

  // Draw the back
  pd3dDevice->SetMaterial (&backTexMaterial);		
  pd3dDevice->SetVertexShader( TFACEVERTEX_FORMAT );
  pd3dDevice->SetIndices( backIdx, 0 );
  pd3dDevice->SetStreamSource( 0, backVbf , sizeof(TFACEVERTEX) );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 20, 0, 10 );

  // Disable texture
  pd3dDevice->SetTexture( 0, NULL );
  pd3dDevice->SetTextureStageState(0, D3DTSS_RESULTARG, D3DTA_CURRENT);
  pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_DISABLE );
  pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

  // Draw the grid (Horizontal line)
  if( !zBuffer ) {
    nbVPlane = ( (width+1)*2 + (height-1)*2 );
    pd3dDevice->SetMaterial (&darkMaterial);		
    pd3dDevice->SetVertexShader( VERTEX_FORMAT );
    pd3dDevice->SetIndices( gridIdx, 0 );
    pd3dDevice->SetStreamSource( 0, gridVbf , sizeof(VERTEX) );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8*depth, 2*nbVPlane, 4*depth );
  }
  break;

  case STYLE_ARCADE:

  // Draw the grid
  pd3dDevice->SetMaterial (&darkMaterial);		
  pd3dDevice->SetVertexShader( VERTEX_FORMAT );
  pd3dDevice->SetIndices( gridIdx, 0 );
  pd3dDevice->SetStreamSource( 0, gridVbf , sizeof(VERTEX) );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, nbGridVertex, 0, nbGridIndex/2 );

  break;
  
  } // End switch(style)

  // Draw filling cubes
  if( renderCube ) {
    for(int i=0;i<mSize;i++) {
      int x = orderMatrix[i].x;
      int y = orderMatrix[i].y;
      int z = orderMatrix[i].z;
      if( GetValue(x,y,z) ) {
        if( zBuffer ) {
          if( IsVisible2(x,y,z) ) 
            RenderCube(pd3dDevice,x,y,z);
        } else {
          if( IsVisible(x,y,z) ) 
            RenderCube(pd3dDevice,x,y,z);
        }
      }
    }
  }

}

//-----------------------------------------------------------------------------

void Pit::InvalidateDeviceObjects() {

    SAFE_RELEASE(backTexture);
    if(backTexture) {delete backTexture;backTexture=NULL;};

    SAFE_RELEASE(cubeTexture);
    if(cubeTexture) {delete cubeTexture;cubeTexture=NULL;};

 	  SAFE_RELEASE(gridVbf);
	  SAFE_RELEASE(gridIdx);

 	  SAFE_RELEASE(cubeVbf);
	  SAFE_RELEASE(cubeIdx);
    for(int i=0;i<NBTCUBE;i++)
	    SAFE_RELEASE(cubeTVbf[i]);

 	  SAFE_RELEASE(lcubeVbf);
    for(int i=0;i<12;i++)
	    SAFE_RELEASE(lcubeIdx[i]);

 	  SAFE_RELEASE(backVbf);
	  SAFE_RELEASE(backIdx);

    SAFE_RELEASE(sideVbf);
	  SAFE_RELEASE(sideIdx);

    inited = FALSE;

}

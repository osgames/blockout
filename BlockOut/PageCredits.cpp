/*
 	File:        PageCredits.cpp
  Description: Credits page
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Menu.h"
// -----------------------------------------------------------

void PageCredits::Prepare(int iParam,void *pParam) {
  nbItem  = 0;
  selItem = 0;
  startTime = 0.0f;
  mParent->GetSound()->PlayMusic();
}

// -----------------------------------------------------------

void PageCredits::Render(LPDIRECT3DDEVICE8 pd3dDevice) {

  mParent->RenderTitle("CREDITS");
	mParent->RenderVersion();

  // Render demo block
  pd3dDevice->SetViewport(&blockView);
  pd3dDevice->SetTransform( D3DTS_WORLD, &matBlock  );
  block.Render(pd3dDevice,FALSE,FALSE);

}

// -----------------------------------------------------------

int PageCredits::Process(BYTE *keys,float fTime) {

  if( keys[VK_ESCAPE] ) {
    mParent->GetSound()->StopMusic();
    mParent->ToPage(&mParent->mainMenuPage);
    keys[VK_ESCAPE] = 0;
  }

  if(startTime==0.0f)
    startTime = fTime;

  float aTime = fTime - startTime;

  D3DXMATRIX t1,t2,rx,ry,rz;

  D3DXMatrixTranslation(&t1,0.0f,0.0f,-0.45f);
  D3DXMatrixRotationX(&rx,aTime * PI/2.0f);
  D3DXMatrixRotationY(&ry,aTime * PI/2.5f);
  D3DXMatrixRotationZ(&rz,aTime * PI/3.0f);
  D3DXMatrixTranslation(&t2,0.0f,0.0f,0.45f);
  D3DXMatrixMultiply(&matBlock,&t1,&rx);
  D3DXMatrixMultiply(&matBlock,&matBlock,&ry);
  D3DXMatrixMultiply(&matBlock,&matBlock,&rz);
  D3DXMatrixMultiply(&matBlock,&matBlock,&t2);

  return 0;
}

// -----------------------------------------------------------

HRESULT PageCredits::Create(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;

  // Get back buffer dimemsion
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  pBackBuffer->Release();

  VERTEX org = v(-0.15f,-0.05f,0.4f);

  block.AddCube(1,0,-1);
  block.AddCube(1,1,-1);
  block.AddCube(1,-1,0);
  block.AddCube(1,0,0);
  block.AddCube(0,0,0);
  hr = block.Create(pd3dDevice,0.1f,org,8);
  if(FAILED(hr)) return hr;

  D3DXMatrixIdentity(&matBlock);

  // block viewport 
  float X = (float)desc.Width  * 0.216f;
  float Y = (float)desc.Height * 0.409f;
  float W = (float)desc.Width  * 0.572f;
  float H = (float)desc.Height * 0.573f;
  
  blockView.X      = fround(X);
  blockView.Y      = fround(Y);
  blockView.Width  = fround(W);
  blockView.Height = fround(H);
  blockView.MinZ   = 0.0f;
  blockView.MaxZ   = 1.0f;

  // Projection matrix
  FLOAT fAspectRatio = 1.0f;
  D3DXMatrixPerspectiveFovRH( &blockProj, D3DXToRadian(60.0f), fAspectRatio, 0.1f, FAR_DISTANCE );

  return S_OK;

}

// -----------------------------------------------------------

void PageCredits::InvalidateDeviceObjects() {

  block.InvalidateDeviceObjects();

}



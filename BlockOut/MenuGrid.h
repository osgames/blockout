/*
 	File:        MenuGrid.h
  Description: Menu graphics management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#define STRICT
#include <math.h>
#include <D3DX8.h>
#include <D3DApp.h>
#include <D3DFont.h>
#include <D3DUtil.h>
#include <DXUtil.h>
#include <D3DObject.h>

#include "Types.h"

#define GRID_WIDTH   70
#define GRID_HEIGHT  50

class MenuGrid {

  public:
    MenuGrid();

    // Initialise device objects
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice);

    // Render the grid
    void Render(LPDIRECT3DDEVICE8 pd3dDevice);

    // Release device objects
    void InvalidateDeviceObjects();

    // Set value
    void SetValue(int x,int y,int value);

    // Clear the grid
    void Clear();

  private:

    HRESULT CreateGrid(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreateCube(LPDIRECT3DDEVICE8 pd3dDevice);
    void RenderCube(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y);

    // Grid variable
    float cSide;
    float startX;
    float startY;
    float startZ;
    int   matrix[GRID_HEIGHT*GRID_WIDTH];
    POINT orderMatrix[GRID_HEIGHT*GRID_WIDTH];

    // Graphics
    LPDIRECT3DVERTEXBUFFER8 gridVbf;
    LPDIRECT3DINDEXBUFFER8  gridIdx;
    int nbGridVertex;
    int nbGridIndex;
    D3DMATERIAL8 gridMaterial;

    LPDIRECT3DVERTEXBUFFER8 redVbf;
    LPDIRECT3DINDEXBUFFER8  redIdx;
    D3DMATERIAL8 redMaterial;

    LPDIRECT3DVERTEXBUFFER8 blueVbf;
    LPDIRECT3DINDEXBUFFER8  blueIdx;
    D3DMATERIAL8 blueMaterial;

    LPDIRECT3DVERTEXBUFFER8 edgeVbf;
    LPDIRECT3DINDEXBUFFER8  topIdx;

    LPDIRECT3DINDEXBUFFER8  nwIdx;
    LPDIRECT3DINDEXBUFFER8  neIdx;
    LPDIRECT3DINDEXBUFFER8  swIdx;
    LPDIRECT3DINDEXBUFFER8  seIdx;

    LPDIRECT3DINDEXBUFFER8  bnIdx;
    LPDIRECT3DINDEXBUFFER8  bwIdx;
    LPDIRECT3DINDEXBUFFER8  bsIdx;
    LPDIRECT3DINDEXBUFFER8  beIdx;

};


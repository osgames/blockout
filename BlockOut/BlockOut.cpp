/*
 	File:        BlockOut.cpp
  Description: Main application class
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "BlockOut.h"
#include "resource.h"

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Entry point to the program. Initializes everything, and goes into a
//       message-processing loop. Idle time is used to render the scene.
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
    BlockOut d3dApp;

    if( FAILED( d3dApp.Create( hInst , d3dApp.theSetup.GetWindowWidth() , 
                                       d3dApp.theSetup.GetWindowHeight() ,
                                       d3dApp.theSetup.GetFullScreen() ,
                                       FALSE,FALSE,IDI_BLOCKICON,d3dApp.GetMultiSample()) ) )
        return 0;

    return d3dApp.Run();
}

//-----------------------------------------------------------------------------
// Name: BlockOut()
// Desc: Application constructor. Sets attributes for the app.
//-----------------------------------------------------------------------------
BlockOut::BlockOut()
{
  inited = FALSE;
  mode = MENU_MODE;
  m_dwCreationWidth  = theSetup.GetWindowWidth();
  m_dwCreationHeight = theSetup.GetWindowHeight();

  m_strWindowTitle    = _T(APP_VERSION);
  m_bUseDepthBuffer   = TRUE;
  m_bLockableBackBuffer = TRUE;

#ifdef _DEBUG
  m_pSmallFont       = new CD3DFont( _T("Arial"),  9, D3DFONT_BOLD );
#endif

  ZeroMemory( m_bKey, sizeof(m_bKey) );
}

//-----------------------------------------------------------------------------
// Name: FrameMove()
// Desc: Called once per frame, the call is the entry point for animating
//       the scene.
//-----------------------------------------------------------------------------
HRESULT BlockOut::UpdateFullScreen()
{
  HRESULT hr = S_OK;

  if( m_bWindowed ) {
    if( theSetup.GetFullScreen() ) {
      // Go to fullscreen
      Pause(TRUE);
      hr = ToggleFullscreen();
      Pause(FALSE);
    }
  } else {
    if( !theSetup.GetFullScreen() ) {
      // Go to windowed
      Pause(TRUE);
      hr = ToggleFullscreen();
      Pause(FALSE);
    }
  }

  return hr;

}

//-----------------------------------------------------------------------------
// Name: GetMultiSample()
// Desc: Returns the D3DMULTISAMPLE_TYPE according to the AA level
//-----------------------------------------------------------------------------
D3DMULTISAMPLE_TYPE BlockOut::GetMultiSample() {

	switch(theSetup.GetAntialiasLevel()) {
    case AA_NONE:
	    return D3DMULTISAMPLE_NONE;
			break;
    case AA_LOW:
	    return D3DMULTISAMPLE_2_SAMPLES;
			break;
    case AA_MEDIUM:
	    return D3DMULTISAMPLE_4_SAMPLES;
			break;
    case AA_HIGH:
	    return D3DMULTISAMPLE_8_SAMPLES;
			break;
	}

	return D3DMULTISAMPLE_NONE;

}

//-----------------------------------------------------------------------------
// Name: FrameMove()
// Desc: Called once per frame, the call is the entry point for animating
//       the scene.
//-----------------------------------------------------------------------------
HRESULT BlockOut::FrameMove()
{

  // General keys

  if( m_bKey[VK_F1] ) {
    BOOL fs = theSetup.GetFullScreen();
    theSetup.SetFullScreen(!fs);
    UpdateFullScreen();
    m_bKey[VK_F1]=0;
  }

  // Processing
  int retValue;
  switch(mode) {
    case MENU_MODE:
      retValue = theMenu.Process(m_bKey,m_fTime);
      switch( retValue ) {
        case 1: // Switch to game mode
          ZeroMemory( m_bKey, sizeof(m_bKey) );
          theGame.StartGame(m_pd3dDevice,m_fTime);
          mode = GAME_MODE;
          break;
        case 2: // Resize
          Resize(theSetup.GetWindowWidth() , theSetup.GetWindowHeight() , TRUE);
          break;
        case 3:
          UpdateFullScreen();
          break;
        case 7:
          theGame.StartDemo(m_pd3dDevice,m_fTime);
          mode = GAME_MODE;
          break;
        case 8:
          theGame.StartPractice(m_pd3dDevice,m_fTime);
          mode = GAME_MODE;
          break;
        case 9:
					Pause(TRUE);
          SetMultiSample(GetMultiSample());
					Pause(FALSE);
          break;
        case 100: // Exit
          InvalidateDeviceObjects();
          exit(0);
          break;
      }
      break;
    case GAME_MODE:
      // Frame limiter
      if( theSetup.GetFrLimiter()!= 0 ) {
        float elapsed = DXUtil_Timer(TIMER_GETABSOLUTETIME) - lastSleepTime;
        float toSleep = (theSetup.GetFrLimitTime() - elapsed)*1000.0f;
        if(toSleep>0.0f) Sleep((DWORD)(toSleep+0.5f));
        lastSleepTime = DXUtil_Timer(TIMER_GETABSOLUTETIME);
      }
      retValue = theGame.Process(m_bKey,m_fTime);
      switch( retValue ) {
        case 1: {
          // Check High Score
          SCOREREC *score = theGame.GetScore();
          SCOREREC *newScore;
          int pos = theSetup.InsertHighScore(score,&newScore);
          if( newScore ) {
            switch(theSetup.GetSoundType()) {
              case SOUND_BLOCKOUT2:
                theSound.PlayWellDone();
                break;
              case SOUND_BLOCKOUT:
                theSound.PlayWellDone2();
                break;
            }
          }
          theMenu.ToPage(&theMenu.hallOfFamePage,pos,(void *)newScore);
          // Switch to menu mode
          mode = MENU_MODE;
        }
        break;
        case 2: // Return from Demo
          ZeroMemory( m_bKey, sizeof(m_bKey) );
          mode = MENU_MODE;
          theMenu.FullRepaint();
          break;
      }
      break;
  }

  return S_OK;
}

//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Called once per frame, the call is the entry point for 3d
//       rendering. This function sets up render states, clears the
//       viewport, and renders the scene.
//-----------------------------------------------------------------------------
HRESULT BlockOut::Render()
{

    if(!inited) return S_OK;

    // Begin the scene
    if( SUCCEEDED( m_pd3dDevice->BeginScene() ) )
    {
        switch( mode ) {
          case MENU_MODE:
            theMenu.Render();
#ifdef _DEBUG
            // Output statistics
            m_pSmallFont->DrawText( 10,  10, D3DCOLOR_ARGB(255,255,255,0), m_strFrameStats );
#endif
            break;
          case GAME_MODE:
            theGame.Render(m_pd3dDevice);
#ifdef _DEBUG
            // Output statistics
            m_pSmallFont->DrawText( 200,  20, D3DCOLOR_ARGB(255,255,255,0), m_strFrameStats );
#endif
            break;
        }

        // End the scene.
        m_pd3dDevice->EndScene();
    }

    return S_OK;
}
//-----------------------------------------------------------------------------
// Name: OneTimeSceneInit()
// Desc: Called during initial app startup, this function performs all the
//       permanent initialization.
//-----------------------------------------------------------------------------
HRESULT BlockOut::OneTimeSceneInit()
{
  char err[1024];

  if( FAILED( theSound.Create( m_hWnd ) ) ) {
    sprintf(err,"Failed to initialize sound manager.\nNo sound will be played.\n%s",theSound.GetErrorMsg());
    MessageBox(NULL,err,"[BlockOut] Warning",MB_OK);
  }

  // Init default
  theSound.SetEnable(theSetup.GetSound());
  theHttp.SetProxy(theSetup.GetUseProxy());
  theHttp.SetProxyName(theSetup.GetProxyName());
  theHttp.SetProxyPort(theSetup.GetProxyPort());

  // Set manager
  theMenu.SetSetupManager(&theSetup);
  theMenu.SetSoundManager(&theSound);
  theMenu.SetHttp(&theHttp);
  theGame.SetSetupManager(&theSetup);
  theGame.SetSoundManager(&theSound);

  return S_OK;
}


//-----------------------------------------------------------------------------
// Name: InitDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------
HRESULT BlockOut::InitDeviceObjects()
{
#ifdef _DEBUG
    m_pSmallFont->InitDeviceObjects( m_pd3dDevice );
#endif
    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: RestoreDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------
HRESULT BlockOut::RestoreDeviceObjects()
{
    HRESULT hr;

#ifdef _DEBUG
    m_pSmallFont->RestoreDeviceObjects();
#endif

    // Setup render states
    m_pd3dDevice->SetRenderState(D3DRS_CULLMODE,  D3DCULL_CW);
    m_pd3dDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD );
  	m_pd3dDevice->SetRenderState(D3DRS_SPECULARENABLE ,  FALSE );
    m_pd3dDevice->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
    m_pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);
    m_pd3dDevice->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, FALSE );
    m_pd3dDevice->SetRenderState(D3DRS_EDGEANTIALIAS, FALSE );
   
    // Create light
    D3DLIGHT8 light;
    ZeroMemory(&light, sizeof(light));

    light.Type        = D3DLIGHT_DIRECTIONAL;
    light.Diffuse.r   = 1.0f;
    light.Diffuse.g   = 1.0f;
    light.Diffuse.b   = 1.0f;
    light.Diffuse.a   = 0.0f;
    light.Specular.r  = 1.0f;
    light.Specular.g  = 1.0f;
    light.Specular.b  = 1.0f;
    light.Specular.a  = 0.0f;
    light.Direction.x = -1.5f;
    light.Direction.y = -1.0f;
    light.Direction.z =  1.0f;

    m_pd3dDevice->SetRenderState( D3DRS_LIGHTING,TRUE );
    m_pd3dDevice->SetLight(0, &light);
    m_pd3dDevice->LightEnable(0, TRUE);
    m_pd3dDevice->SetRenderState( D3DRS_AMBIENT, D3DCOLOR_COLORVALUE( 1.0, 1.0, 1.0, 1.0 ) );

    // Set up the view matrix
    D3DXMATRIX  matView;
    D3DXVECTOR3 vEye = D3DXVECTOR3(  0.0f, 0.0f,  0.0f );  // Camera pos
    D3DXVECTOR3 vAt  = D3DXVECTOR3(  0.0f, 0.0f, 10.0f );  // Camara target
    D3DXVECTOR3 vUp  = D3DXVECTOR3(  0.0f, 1.0f,  0.0f );  // Up vector
    D3DXMatrixLookAtRH( &matView, &vEye, &vAt, &vUp );
    m_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

    // Set up the default world matrix
    D3DXMATRIX identity;
    D3DXMatrixIdentity(&identity);
    m_pd3dDevice->SetTransform( D3DTS_WORLD, &identity );

    // Set up device objects
    hr = theMenu.Create(m_pd3dDevice);
    if(FAILED(hr)) return hr;

    hr = theGame.Create(m_pd3dDevice);
    theGame.SetViewMatrix(&matView);
    if(FAILED(hr)) return hr;

    inited = TRUE;
    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: InvalidateDeviceObjects()
// Desc:
//-----------------------------------------------------------------------------
HRESULT BlockOut::InvalidateDeviceObjects()
{
    inited = FALSE;
#ifdef _DEBUG
    m_pSmallFont->InvalidateDeviceObjects();
#endif
    theGame.InvalidateDeviceObjects();
    theMenu.InvalidateDeviceObjects();

    // Test if the device has been lost and reload texture
    HRESULT hr;
    if( FAILED( hr = m_pd3dDevice->TestCooperativeLevel() ) )
    {
      if( D3DERR_DEVICELOST == hr )
        D3DTexture::Destroy();
    }

    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: DeleteDeviceObjects()
// Desc: Called when the app is exiting, or the device is being changed,
//       this function deletes any device dependent objects.
//-----------------------------------------------------------------------------
HRESULT BlockOut::DeleteDeviceObjects()
{
#ifdef _DEBUG
    m_pSmallFont->DeleteDeviceObjects();
#endif
    D3DTexture::Destroy();
    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: FinalCleanup()
// Desc: Called before the app exits, this function gives the app the chance
//       to cleanup after itself.
//-----------------------------------------------------------------------------
HRESULT BlockOut::FinalCleanup()
{
#ifdef _DEBUG
    SAFE_DELETE( m_pSmallFont );
#endif
    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: ConfirmDevice()
// Desc: Called during device intialization, this code checks the device
//       for some minimum set of capabilities
//-----------------------------------------------------------------------------
HRESULT BlockOut::ConfirmDevice( D3DCAPS8* pCaps, DWORD dwBehavior,
                                          D3DFORMAT Format )
{
    if( (dwBehavior & D3DCREATE_HARDWARE_VERTEXPROCESSING ) ||
        (dwBehavior & D3DCREATE_MIXED_VERTEXPROCESSING ) )
    {
        if( pCaps->VertexShaderVersion < D3DVS_VERSION(1,0) )
            return E_FAIL;
    }

    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: Message proc function to handle key and menu input
//-----------------------------------------------------------------------------
LRESULT BlockOut::MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam,
                                    LPARAM lParam )
{
    // Handle key presses
    switch( uMsg )
    {
        case WM_PAINT:
            // Need a full repaint
            theGame.FullRepaint();
            theMenu.FullRepaint();
            break;

        case WM_KEYDOWN:
            m_bKey[wParam] = 1;
            break;

        case WM_KEYUP:
            if( wParam == VK_SHIFT )
              m_bKey[wParam] = 0;
            break;
    }

    // Pass remaining messages to default handler
    return CD3DApplication::MsgProc( hWnd, uMsg, wParam, lParam );
}

/*
  File:        AITester.cpp
  Description: AI player (Testing,parameters scan)
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "BotPlayer.h"
#include <stdio.h>
#include <math.h>
#include <time.h>
#ifndef WIN32
#include <sys/time.h>
#endif

#define MAX_AVG 300

// Globals -----------------------------------------------

BotPlayer *bot;
FILE      *f;
double     avgHist[MAX_AVG];
int        nbAvg;

// -------------------------------------------------------

long get_ticks() {
  
  static int tickStart = -1;
    
#ifdef WIN32

  if(tickStart < 0 )
    tickStart = GetTickCount();
    
  return (GetTickCount() - tickStart);
  
#else

  if(tickStart < 0 )
    tickStart = time(NULL);
    
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

#endif

}

// -------------------------------------------------------

void getAvgBounds(double *min,double *max) {

  *min =  1e20f;
  *max = -1e20f;
  for(int i=0;i<nbAvg;i++) {
    if(avgHist[i]<*min) *min=avgHist[i];
    if(avgHist[i]>*max) *max=avgHist[i];
  }
  
}

// -------------------------------------------------------

void addToAvgHist(double avg) {

  if(nbAvg<MAX_AVG) {
    avgHist[nbAvg] = avg;
    nbAvg++;
  } else {
    // Shift avg history
    for(int i=0;i<nbAvg-1;i++)
      avgHist[i] = avgHist[i+1];
    avgHist[nbAvg-1] = avg;
  }

}

// -------------------------------------------------------

double playGames(int nbGame,int show,int cubeLimit) {

  int min = 2000000000;
  int max = 0;
  int sum = 0;
  int minSeed = 0;
  int seed;
  double minAvg;
  double maxAvg;
  double avg;
  nbAvg=0;

  for(int g=0;g<nbGame;g++) {

    int nb = bot->Test(cubeLimit,&seed);
    sum += nb;
    if(nb>max) max=nb;
    if(nb<min) {
      min=nb;
      minSeed=seed;
    }

    if(show) {
      avg = (double)sum/(double)(g+1);
      addToAvgHist(avg);
      getAvgBounds(&minAvg,&maxAvg);
      printf("[%4d] %6d Avg=%.2f [%.2f,%.2f] (Min %d Max %d)\n",g,nb,avg,minAvg,maxAvg,min,max);
      //fprintf(f,"[%4d] %6d Avg=%.2f [%.2f,%.2f] (Min %d Max %d)\n",g,nb,avg,minAvg,maxAvg,min,max);
    }

  }

  avg = (double)sum/(double)nbGame;
  if(show) {
    printf("Avg=%g Min=%d(%d) Max=%d nbGame=%d\n",
      avg,
      min,
      minSeed,
      max,
      nbGame);
      /*
    fprintf(f,"Avg=%g Min=%d(%d) Max=%d nbGame=%d\n",
      avg,
      min,
      minSeed,
      max,
      nbGame);
     */
  }
  
  return (double)sum;

}

// -------------------------------------------------------

void printTime(int s) {

  int min = (int)s / 60;
  int sec = (int)s % 60;
  printf("%dmin %02dsec",min,sec);

}


// -------------------------------------------------------

void makeScan(char *name,float *coef,float min,float max,int step,int nbGame) {

  float dx = (max-min)/(float)step;
  int nbScan = step+1;
  char filename[256];
  sprintf(filename,"%s.scan",name);

  printf("-----------------------------------------------------\n");
  printf("Scanning %s (%.3f => %.3f) %d steps\n",name,min,max,nbScan);
  printf("-----------------------------------------------------\n");
  f=fopen(filename,"w");
  fprintf(f,"#%s\n",name);
  for(int i=0;i<nbScan;i++) {
    *coef = min + dx * (float)i;
    printf("(%d/%d) %.3f =>",i+1,nbScan,*coef);
    double sum = playGames(nbGame,false,1000000);
    double avg = sum/(double)nbGame;
    printf("%.2f\n",avg);
    fprintf(f,"%g %g\n",*coef,avg);
  }
  fclose(f);

}

// -------------------------------------------------------

int main(int argc, char* argv[])
{
  bot = new BotPlayer();

  long t0 = get_ticks();

  double nbC = 0.0f;

  /*
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("smooth",&(bot->smoothCoef),-0.4,-0.1,30,5000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("lines",&(bot->linesCoef),0.0,2.0,20,10000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("holes",&(bot->holeCoef),-1.5,-0.5,10,10000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("corner",&(bot->cornerCoef),2.0,3.0,10,10000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("peak",&(bot->peakCoef),-1.0,0.0,10,5000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("edges",&(bot->edgeCoef),0.5,1.5,10,5000);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  makeScan("puzzle",&(bot->puzzleCoef),3.0,15.0,50,10000);
  */
  
  //f=fopen("avg.txt","w");
  //bot->Init(3,3,6,BLOCKSET_FLAT);
  bot->Init(3,3,6,BLOCKSET_BASIC);
  //bot->Init(5,5,10,BLOCKSET_EXTENDED);
  nbC = playGames(1000,true,4000000);
  //fclose(f);

  long t1 =get_ticks();
  double s =(double)(t1-t0)/1000.0;
  double bps = nbC / s;
    
  printTime((int)((double)(t1-t0)/1000.0+0.5));
  printf(", %g cps \n",bps);

  return 0;
}


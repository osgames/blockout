/*
 	File:        BlockOut.h
  Description: Main application class
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Game.h"
#include "Menu.h"
#include "SetupManager.h"
#include "SoundManager.h"
#include "Http.h"

#define MENU_MODE 1
#define GAME_MODE 2

class BlockOut : public CD3DApplication
{

#ifdef _DEBUG
    // Font for drawing text
    CD3DFont* m_pSmallFont;
#endif

    // Navigation
    BYTE        m_bKey[256];

    // Global mode
    BOOL inited;
    int  mode;
    float lastSleepTime;

protected:
    HRESULT OneTimeSceneInit();
    HRESULT InitDeviceObjects();
    HRESULT RestoreDeviceObjects();
    HRESULT InvalidateDeviceObjects();
    HRESULT DeleteDeviceObjects();
    HRESULT FinalCleanup();
    HRESULT Render();
    HRESULT FrameMove();
    HRESULT ConfirmDevice( D3DCAPS8* pCaps, DWORD dwBehavior, D3DFORMAT Format );
    LRESULT MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
    HRESULT UpdateFullScreen();

public:
    BlockOut();

    // Global handles
    Game theGame;
    Menu theMenu;
    SetupManager theSetup;
    SoundManager theSound;
    Http         theHttp;

		D3DMULTISAMPLE_TYPE GetMultiSample();

};

/*
 	File:        MenuGrid.cpp
  Description: Menu graphics management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "MenuGrid.h"

// ----------------------------------------------------------------

MenuGrid::MenuGrid() {

  gridVbf = NULL;
  gridIdx = NULL;
  nbGridVertex = 0;
  nbGridIndex = 0;
  memset(matrix,0,sizeof(matrix));

  // Initialiase order matrix
  int i,j;
  int dist[GRID_WIDTH*GRID_HEIGHT];
  for(i=0;i<GRID_WIDTH;i++) {
    for(j=0;j<GRID_HEIGHT;j++) {
      orderMatrix[i + GRID_WIDTH*j].x = i;
      orderMatrix[i + GRID_WIDTH*j].y = j;
      dist[i + GRID_WIDTH*j] = (i-GRID_WIDTH/2) * (i-GRID_WIDTH/2) + (j-GRID_HEIGHT/2) * (j-GRID_HEIGHT/2);
    }
  }

  // Sort
  BOOL end = FALSE;
  i=0;
  j=GRID_WIDTH*GRID_HEIGHT-1;

  while(!end) {
    end = TRUE;
    for(i=0;i<j;i++) {
      if( dist[i]<dist[i+1] ) {

        // Swap dist
        int tmp = dist[i];
        dist[i] = dist[i+1];
        dist[i+1] = tmp;
        // Swap orderMatrix
        POINT tmp2 = orderMatrix[i];
        orderMatrix[i] = orderMatrix[i+1];
        orderMatrix[i+1] = tmp2;

        end = FALSE;
      }
    }
    j--;
  }

}

// ----------------------------------------------------------------

void MenuGrid::Clear() {
  memset(matrix,0,sizeof(matrix));
}

// ----------------------------------------------------------------

void MenuGrid::SetValue(int x,int y,int value) {
  matrix[x + GRID_WIDTH*y] = value;
}

// ----------------------------------------------------------------

HRESULT MenuGrid::Create(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;

  // Global var
  cSide  = 0.1f;
  startX = -(float)GRID_WIDTH  * cSide / 2.0f;
  startY = -(float)GRID_HEIGHT * cSide / 2.0f;
  startZ = 3.0f + cSide;

  hr = CreateGrid(pd3dDevice);
  if( FAILED(hr) ) return hr;

  hr = CreateCube(pd3dDevice);
  if( FAILED(hr) ) return hr;

  return S_OK;

}

// ----------------------------------------------------------------

HRESULT MenuGrid::CreateGrid(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;

  // Material
  memset (&gridMaterial, 0, sizeof (D3DMATERIAL8));
	gridMaterial.Diffuse.r = 1.0f;
	gridMaterial.Diffuse.g = 1.0f;
	gridMaterial.Diffuse.b = 1.0f;
	gridMaterial.Ambient.r = 0.0f;
	gridMaterial.Ambient.g = 0.5f;
	gridMaterial.Ambient.b = 0.0f;

  // Grid vertex
  VERTEX *pVertices;

  nbGridVertex = 2*(GRID_WIDTH+1) + 2*(GRID_HEIGHT+1);

  hr = pd3dDevice->CreateVertexBuffer(nbGridVertex * sizeof(VERTEX), 
            D3DUSAGE_DYNAMIC, VERTEX_FORMAT, D3DPOOL_DEFAULT, &gridVbf);
  if(FAILED(hr)) return hr;

  hr = gridVbf->Lock(0, nbGridVertex * sizeof(VERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  int l = 0;
  for(int i=0;i<=GRID_WIDTH;i++) {
    pVertices[l++] = v( startX + (i*cSide) - cSide/2.0f, -startY , startZ );
    pVertices[l++] = v( startX + (i*cSide) - cSide/2.0f,  startY , startZ );
  }

  for(int i=0;i<=GRID_HEIGHT;i++) {
    pVertices[l++] = v( -startX - cSide/2.0f, startY + (i*cSide) , startZ );
    pVertices[l++] = v(  startX - cSide/2.0f, startY + (i*cSide) , startZ );
  }

  gridVbf->Unlock();

  // Indices
  WORD *pIndices;

  nbGridIndex = nbGridVertex;

  hr = pd3dDevice->CreateIndexBuffer(nbGridIndex * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &gridIdx);
  if(FAILED(hr)) return hr;

  hr = gridIdx->Lock(0, nbGridIndex * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l = 0;
  for(int i=0;i<=GRID_WIDTH;i++) {
    pIndices[l++] = 2*i;
    pIndices[l++] = 2*i+1;
  }

  for(int i=0;i<=GRID_HEIGHT;i++) {
    pIndices[l++] = 2*(GRID_WIDTH+1) + 2*i;
    pIndices[l++] = 2*(GRID_WIDTH+1) + 2*i+1;
  }

  gridIdx->Unlock();

  return S_OK;
}

// ----------------------------------------------------------------

HRESULT MenuGrid::CreateCube(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;
  int l = 0;
  FACEVERTEX *pVertices;
  VERTEX     *lVertices;
  WORD       *pIndices;

  // Material
  memset (&redMaterial, 0, sizeof (D3DMATERIAL8));
	redMaterial.Diffuse.r = 0.5f;
	redMaterial.Diffuse.g = 0.0f;
	redMaterial.Diffuse.b = 0.0f;
	redMaterial.Ambient.r = 1.0f;
	redMaterial.Ambient.g = 0.0f;
	redMaterial.Ambient.b = 0.0f;

  // Red face
  hr = pd3dDevice->CreateVertexBuffer(4 * sizeof(FACEVERTEX), 
            D3DUSAGE_DYNAMIC, FACEVERTEX_FORMAT, D3DPOOL_DEFAULT, &redVbf);
  if(FAILED(hr)) return hr;

  hr = redVbf->Lock(0, 4 * sizeof(FACEVERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  pVertices[0] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                    0,0,-1 );

  pVertices[1] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                    0,0,-1 );

  pVertices[2] = v( 1 * cSide + startX,
                    0 * cSide + startY,
                   -1 * cSide + startZ,
                    0,0,-1 );

  pVertices[3] = v( 0 * cSide + startX,
                    0 * cSide + startY,
                   -1 * cSide + startZ,
                    0,0,-1 );

  redVbf->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(6 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &redIdx);
  if(FAILED(hr)) return hr;

  hr = redIdx->Lock(0, 6 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[l++] =  0;  pIndices[l++] =  1;  pIndices[l++] =  2;
  pIndices[l++] =  0;  pIndices[l++] =  2;  pIndices[l++] =  3;

  redIdx->Unlock();

  // Material
  memset (&blueMaterial, 0, sizeof (D3DMATERIAL8));
	blueMaterial.Diffuse.r = 0.0f;
	blueMaterial.Diffuse.g = 0.0f;
	blueMaterial.Diffuse.b = 1.0f;
	blueMaterial.Ambient.r = 0.2f;
	blueMaterial.Ambient.g = 0.2f;
	blueMaterial.Ambient.b = 0.7f;

  // Blue faces
  hr = pd3dDevice->CreateVertexBuffer(16 * sizeof(FACEVERTEX), 
            D3DUSAGE_DYNAMIC, FACEVERTEX_FORMAT, D3DPOOL_DEFAULT, &blueVbf);
  if(FAILED(hr)) return hr;

  hr = blueVbf->Lock(0, 16 * sizeof(FACEVERTEX), (BYTE **) &pVertices, 0);
  if(FAILED(hr)) return hr;

  hr = pd3dDevice->CreateIndexBuffer(24 * sizeof(WORD), 0, D3DFMT_INDEX16,
		                                   D3DPOOL_DEFAULT, &blueIdx);
  if(FAILED(hr)) return hr;

  hr = blueIdx->Lock(0, 24 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  l = 0;
  // Face 1

  pVertices[0] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                    1,0,0 );

  pVertices[1] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ,
                    1,0,0 );

  pVertices[2] = v( 1 * cSide + startX,
                    0 * cSide + startY,
                    0 * cSide + startZ,
                    1,0,0 );

  pVertices[3] = v( 1 * cSide + startX,
                    0 * cSide + startY,
                   -1 * cSide + startZ,
                    1,0,0 );

  pIndices[l++] =  0;  pIndices[l++] =  1;  pIndices[l++] =  2;
  pIndices[l++] =  0;  pIndices[l++] =  2;  pIndices[l++] =  3;

  // Face 2

  pVertices[4] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ,
                    0,1,0 );

  pVertices[5] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ,
                    0,1,0 );

  pVertices[6] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                    0,1,0 );

  pVertices[7] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                    0,1,0 );


  pIndices[l++] =  4;  pIndices[l++] =  5;  pIndices[l++] = 6;
  pIndices[l++] =  4;  pIndices[l++] =  6;  pIndices[l++] = 7;

  // Face 3

  pVertices[8] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ,
                   -1,0,0 );

  pVertices[9] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ,
                   -1,0,0 );

  pVertices[10] = v( 0 * cSide + startX,
                     0 * cSide + startY,
                    -1 * cSide + startZ,
                    -1,0,0 );

  pVertices[11] = v( 0 * cSide + startX,
                     0 * cSide + startY,
                     0 * cSide + startZ,
                    -1,0,0 );

  pIndices[l++] =  8;  pIndices[l++] = 9;  pIndices[l++] = 10;
  pIndices[l++] =  8;  pIndices[l++] = 10;  pIndices[l++] = 11;

  // Face 4

  pVertices[12] = v( 1 * cSide + startX,
                     0 * cSide + startY,
                    -1 * cSide + startZ,
                     0,-1,0 );

  pVertices[13] = v( 1 * cSide + startX,
                     0 * cSide + startY,
                     0 * cSide + startZ,
                     0,-1,0 );

  pVertices[14] = v( 0 * cSide + startX,
                     0 * cSide + startY,
                     0 * cSide + startZ,
                     0,-1,0 );

  pVertices[15] = v( 0 * cSide + startX,
                     0 * cSide + startY,
                    -1 * cSide + startZ,
                     0,-1,0 );

  pIndices[l++] =  12;  pIndices[l++] = 13;  pIndices[l++] = 14;
  pIndices[l++] =  12;  pIndices[l++] = 14;  pIndices[l++] = 15;

  blueVbf->Unlock();
  blueIdx->Unlock();

  // Edge
  hr = pd3dDevice->CreateVertexBuffer(8 * sizeof(VERTEX), 
            D3DUSAGE_DYNAMIC, VERTEX_FORMAT, D3DPOOL_DEFAULT, &edgeVbf);
  if(FAILED(hr)) return hr;

  hr = edgeVbf->Lock(0, 8 * sizeof(VERTEX), (BYTE **) &lVertices, 0);
  if(FAILED(hr)) return hr;

  lVertices[0] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ);

  lVertices[1] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                   -1 * cSide + startZ);

  lVertices[2] = v( 1 * cSide + startX,
                    0 * cSide + startY,
                   -1 * cSide + startZ);

  lVertices[3] = v( 0 * cSide + startX,
                    0 * cSide + startY,
                   -1 * cSide + startZ);

  lVertices[4] = v( 0 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ);

  lVertices[5] = v( 1 * cSide + startX,
                    1 * cSide + startY,
                    0 * cSide + startZ);

  lVertices[6] = v( 1 * cSide + startX,
                    0 * cSide + startY,
                    0 * cSide + startZ);

  lVertices[7] = v( 0 * cSide + startX,
                    0 * cSide + startY,
                    0 * cSide + startZ);

  edgeVbf->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(8 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &topIdx);
  if(FAILED(hr)) return hr;

  hr = topIdx->Lock(0, 8 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 0;   pIndices[1] = 1;
  pIndices[2] = 1;   pIndices[3] = 2;
  pIndices[4] = 2;   pIndices[5] = 3;
  pIndices[6] = 3;   pIndices[7] = 0;

  topIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &nwIdx);
  if(FAILED(hr)) return hr;

  hr = nwIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 1;   pIndices[1] = 5;

  nwIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &neIdx);
  if(FAILED(hr)) return hr;

  hr = neIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 0;   pIndices[1] = 4;

  neIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &swIdx);
  if(FAILED(hr)) return hr;

  hr = swIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 2;   pIndices[1] = 6;

  swIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &seIdx);
  if(FAILED(hr)) return hr;

  hr = seIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 3;   pIndices[1] = 7;

  seIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &bnIdx);
  if(FAILED(hr)) return hr;

  hr = bnIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 4;   pIndices[1] = 5;

  bnIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &bwIdx);
  if(FAILED(hr)) return hr;

  hr = bwIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 5;   pIndices[1] = 6;

  bwIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &bsIdx);
  if(FAILED(hr)) return hr;

  hr = bsIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 6;   pIndices[1] = 7;

  bsIdx->Unlock();

  hr = pd3dDevice->CreateIndexBuffer(2 * sizeof(WORD), 0, D3DFMT_INDEX16,
	  	                                   D3DPOOL_DEFAULT, &beIdx);
  if(FAILED(hr)) return hr;

  hr = beIdx->Lock(0, 2 * sizeof(WORD), (BYTE**) &pIndices, 0);
  if(FAILED(hr)) return hr;

  pIndices[0] = 7;   pIndices[1] = 4;

  beIdx->Unlock();

  return S_OK;
}

// ----------------------------------------------------------------

void MenuGrid::RenderCube(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y) {

  D3DXMATRIX matPos;

  D3DXMatrixTranslation(&matPos,x*cSide - cSide/2.0f,y*cSide,0.0f);
  pd3dDevice->SetTransform( D3DTS_WORLD , &matPos);

  // Draw the blue faces
  pd3dDevice->SetMaterial( &blueMaterial );
  pd3dDevice->SetVertexShader( FACEVERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, blueVbf , sizeof(FACEVERTEX) );
  pd3dDevice->SetIndices( blueIdx, 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 16, 0, 8 );

  // Draw the red face
  pd3dDevice->SetMaterial( &redMaterial );
  pd3dDevice->SetVertexShader( FACEVERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, redVbf , sizeof(FACEVERTEX) );
  pd3dDevice->SetIndices( redIdx, 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 4, 0, 2 );

  // Top edge
  pd3dDevice->SetMaterial( &gridMaterial );
  pd3dDevice->SetVertexShader( VERTEX_FORMAT );
  pd3dDevice->SetStreamSource( 0, edgeVbf , sizeof(VERTEX) );
  pd3dDevice->SetIndices( topIdx, 0 );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 4 );

  if( x<GRID_WIDTH/2 || y<GRID_HEIGHT/2) {
    pd3dDevice->SetIndices( nwIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

  if( x>GRID_WIDTH/2 || y<GRID_HEIGHT/2) {
    pd3dDevice->SetIndices( neIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

  if( x<GRID_WIDTH/2 || y>GRID_HEIGHT/2) {
    pd3dDevice->SetIndices( swIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

  if( x>GRID_WIDTH/2 || y>GRID_HEIGHT/2) {
    pd3dDevice->SetIndices( seIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

  if( x>GRID_WIDTH/2 ) {
    pd3dDevice->SetIndices( beIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  } else {
    pd3dDevice->SetIndices( bwIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

  if( y>GRID_HEIGHT/2 ) {
    pd3dDevice->SetIndices( bsIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  } else {
    pd3dDevice->SetIndices( bnIdx, 0 );
    pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, 8, 0, 1 );
  }

}

// ----------------------------------------------------------------

void MenuGrid::Render(LPDIRECT3DDEVICE8 pd3dDevice) {

  D3DXMATRIX mI;
  D3DXMatrixIdentity(&mI);

  // Render grid
  pd3dDevice->SetTransform(D3DTS_WORLD,&mI);
  pd3dDevice->SetMaterial(&gridMaterial);		
  pd3dDevice->SetVertexShader( VERTEX_FORMAT );
  pd3dDevice->SetIndices( gridIdx, 0 );
  pd3dDevice->SetStreamSource( 0, gridVbf , sizeof(VERTEX) );
  pd3dDevice->DrawIndexedPrimitive( D3DPT_LINELIST, 0, nbGridVertex, 0, nbGridIndex/2 );

  // Render cubes
  for(int i=0;i<GRID_WIDTH*GRID_HEIGHT;i++) {
    int x = orderMatrix[i].x;
    int y = orderMatrix[i].y;
    if( matrix[x + GRID_WIDTH*y] ) {
      RenderCube(pd3dDevice,x,y);
    }
  }

}

// ----------------------------------------------------------------

void MenuGrid::InvalidateDeviceObjects() {

 	  SAFE_RELEASE(gridVbf);
	  SAFE_RELEASE(gridIdx);
 	  SAFE_RELEASE(blueVbf);
	  SAFE_RELEASE(blueIdx);
 	  SAFE_RELEASE(redVbf);
	  SAFE_RELEASE(redIdx);
 	  SAFE_RELEASE(edgeVbf);
 	  SAFE_RELEASE(topIdx);
 	  SAFE_RELEASE(nwIdx);
 	  SAFE_RELEASE(neIdx);
 	  SAFE_RELEASE(swIdx);
 	  SAFE_RELEASE(seIdx);
 	  SAFE_RELEASE(bnIdx);
 	  SAFE_RELEASE(bwIdx);
 	  SAFE_RELEASE(bsIdx);
 	  SAFE_RELEASE(beIdx);

}


/*
 	File:        Types.h 
  Description: Contains routines for accessing HTTP sites
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/


#include "Http.h"
#include <stdio.h>

// -------------------------------------------------------

Http::Http() {
  strcpy(err_str,"No error");
  useProxy = FALSE;
  strcpy(proxyName,"");
  proxyPort=0;
}

// -------------------------------------------------------

void Http::SetProxy(BOOL enable) {
  useProxy = enable;
}

// -------------------------------------------------------

void Http::SetProxyName(char *name) {
  strncpy(proxyName,name,255);
  proxyName[255]=0;
}

// -------------------------------------------------------

void Http::SetProxyPort(int port) {
  proxyPort = port;
}

// -------------------------------------------------------

BOOL Http::CheckProxy() {

  if( useProxy ) {

    if( strlen(proxyName)==0 ) {
      sprintf(err_str,"Invalid HTTP proxy name");
      return FALSE;
    }

    if( proxyPort<=0 || proxyPort>65535 ) {
      sprintf(err_str,"Invalid HTTP proxy port number");
      return FALSE;
    }

  }

  return TRUE;

}

// -------------------------------------------------------

void Http::buildErrorMessage(DWORD errorCode) {

  DWORD  dwLegnth;

  if( errorCode == ERROR_INTERNET_EXTENDED_ERROR ) {

    // extended error handling
    char  eBuff[1024];
    DWORD bLength=1024;
    DWORD iErrorCode=0;

    if( InternetGetLastResponseInfo(&iErrorCode,eBuff,&bLength) )
    {
      sprintf(err_str,"%s",eBuff);
    } else {
	    sprintf(err_str,"InternetGetLastResponseInfo() failed");
	  }

    return;

  }

  // Classic error handling
  char lpMsgBuf[512];

	dwLegnth = FormatMessage(
		      FORMAT_MESSAGE_FROM_HMODULE | FORMAT_MESSAGE_FROM_SYSTEM,
          GetModuleHandle( "wininet.dll" ),
		      errorCode,
		      0,	// Default language 
		      (LPTSTR) & lpMsgBuf,
		      512,
          NULL );

  if( dwLegnth == 0 ) {
    sprintf(err_str,"Error code=%d",GetLastError());
  } else {
    sprintf(err_str,"%s",(LPTSTR)lpMsgBuf);	 
  }

}

// -------------------------------------------------------

BOOL Http::SetTimeout(HINTERNET iSession,DWORD timeout) {

  if( !InternetSetOption(iSession,INTERNET_OPTION_CONNECT_TIMEOUT,&timeout,4) ) {
	  return FALSE;
  }

  if( !InternetSetOption(iSession,INTERNET_OPTION_RECEIVE_TIMEOUT,&timeout,4) ) {
	  return FALSE;
  }

  if( !InternetSetOption(iSession,INTERNET_OPTION_SEND_TIMEOUT,&timeout,4) ) {
	  return FALSE;
  }

  return TRUE;

}

// -------------------------------------------------------

char *Http::UploadFile(char *url,char *remotePHP,BYTE *buffer,DWORD length,DWORD timeout)
{

  const char *acceptFile[] = { "text/*" , NULL };

  DWORD dwBytesRead;
  HINTERNET iSession;

  if( !CheckProxy() ) {
    return NULL;
  }

  // ---- Open Internet session -----------------------------------------------------------

  if( !useProxy ) {
   iSession = InternetOpen("BlockOut",INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,0);
  } else {
   char proxy[512];
   sprintf(proxy,"%s:%d",proxyName,proxyPort);
   iSession = InternetOpen("BlockOut",INTERNET_OPEN_TYPE_PROXY,proxy,NULL,0);
  }

  if( iSession == NULL ) {

	  buildErrorMessage(GetLastError());
	  return NULL;

  }

  // ---- Set timeout ------------------------------------------------------------

  if( !SetTimeout(iSession,timeout) ) {

	  buildErrorMessage(GetLastError());
    InternetCloseHandle(iSession);
	  return NULL;

  }

  // ---- Connect to the URL -----------------------------------------------------------

  HINTERNET hSession = InternetConnect(iSession,url,INTERNET_DEFAULT_HTTP_PORT,NULL,NULL,
                                       INTERNET_SERVICE_HTTP,0,NULL);

  if( hSession == NULL ) {

	  buildErrorMessage(GetLastError());
    InternetCloseHandle(iSession);
	  return NULL;

  }

  // ---- Open the request ----------------------------------------------------------

  HINTERNET hRequest = HttpOpenRequest (hSession, "PUT",  remotePHP, NULL, NULL, acceptFile ,  0, 0);
  if (!hRequest)
  {
    buildErrorMessage(GetLastError());
    InternetCloseHandle(hSession);
    InternetCloseHandle(iSession);
    return NULL;
  }

  // ---- Send the request ----------------------------------------------------------

  if(!HttpSendRequest( hRequest, NULL , 0 , buffer, length))
  {
     buildErrorMessage(GetLastError());
     HttpEndRequest(hRequest, NULL, 0, 0);
     InternetCloseHandle(hSession);
     InternetCloseHandle(iSession);
     return NULL;
  }

  // ---- Get response --------------------------------------------------------------

  dwBytesRead = 262143;
  if( !HttpQueryInfo(hRequest,HTTP_QUERY_RAW_HEADERS_CRLF,response,&dwBytesRead,NULL) ) {
 	   buildErrorMessage(GetLastError());
     HttpEndRequest(hRequest, NULL, 0, 0);
     InternetCloseHandle(hSession);
     InternetCloseHandle(iSession);
     return NULL;
  }
  response[dwBytesRead]=0;

  // ---- Extract response ----------------------------------------------------
  char tmp[256];
  int  i=0;
  char *r = strstr(response,"RecResp:");
  if( r==NULL ) {
     sprintf(err_str,"Registering error");
     HttpEndRequest(hRequest, NULL, 0, 0);
     InternetCloseHandle(hSession);
     InternetCloseHandle(iSession);
     return NULL;
  }

  r += 8;
  if(*r==32) r++;
  while( *r>=32 ) {
    tmp[i] = *r;
    r++;i++;
  }
  tmp[i]=0;
  strcpy(response,tmp);

  // ---- Close the request ----------------------------------------------------------

  if(!HttpEndRequest(hRequest, NULL, 0, 0))
  {
    buildErrorMessage(GetLastError());
    InternetCloseHandle(hSession);
    InternetCloseHandle(iSession);
    return NULL;
  }

  // ---- End of upload --------------------------------------------------------------

  InternetCloseHandle(hSession);
  InternetCloseHandle(iSession);
  return response;

}

// -------------------------------------------------------

char *Http::Get(char *link,DWORD timeout,DWORD *outLength) {

  HINTERNET iSession;
  HINTERNET hSession;

  if( !CheckProxy() ) {
    return NULL;
  }

  // ---- Open Internet session -----------------------------------------------------------

  if( !useProxy ) {
   iSession = InternetOpen("BlockOut",INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,0);
  } else {
   char proxy[256];
   sprintf(proxy,"%s:%d",proxyName,proxyPort);
   iSession = InternetOpen("BlockOut",INTERNET_OPEN_TYPE_PROXY,proxy,NULL,0);
  }

  if( iSession == NULL ) {

	  buildErrorMessage(GetLastError());
	  return NULL;

  }

  // ---- Set timeout ------------------------------------------------------------

  if( !SetTimeout(iSession,timeout) ) {

	  buildErrorMessage(GetLastError());
    InternetCloseHandle(iSession);
	  return NULL;

  }

  // ---- Open URL -----------------------------------------------------------------

  hSession = InternetOpenUrl(iSession,link,NULL,0,INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_NO_CACHE_WRITE,NULL);
  if( hSession==NULL ) {

	  buildErrorMessage(GetLastError());
	  InternetCloseHandle(iSession);
	  return FALSE;

  }

  // ---- Read the response -----------------------------------------------------

  DWORD nbRead;
  BOOL  readOK;
  int   pos=0;

  do {
   readOK = InternetReadFile(hSession,response+pos,262143-pos,&nbRead);
   if(readOK) pos += nbRead;
  } while( readOK && nbRead>0 );

  response[pos]=0;
  if(outLength) *outLength = pos;
  InternetCloseHandle(hSession);
  InternetCloseHandle(iSession);
  return response;

}

// -------------------------------------------------------

char *Http::GetError() {

  return err_str;

}

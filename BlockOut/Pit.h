/*
 	File:        Pit.h
  Description: Pit management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "PolyCube.h"

#define NBTCUBE 16

class Pit
{
  public:
    Pit();

    // Initialise pit dimension
    void SetDimension(int gWidth,int gHeight,int gDepth);

    // Get dimension
    int GetWidth();
    int GetHeight();
    int GetDepth();

    // Create Pit device objects
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice,int style);

    // Return the cubeSide (3D space)
    float GetCubeSide();

    // Return the Origin (3D space)
    VERTEX GetOrigin();

    // Render the pit
    void Render(LPDIRECT3DDEVICE8 pd3dDevice,BOOL renderCube,BOOL zBuffer);

    // Render the pit level
    void RenderLevel(LPDIRECT3DDEVICE8 pd3dDevice,LPDIRECT3DSURFACE8 backBuffer);

    // Release device objects
    void InvalidateDeviceObjects();
    
    // Clear the pit
    void Clear();
    
    // Get the value at the specified coordinates
    // Return 1 when (x,y,z) if out of the pit
    int GetValue(int x,int y,int z);

    // Get the value at the specified coordinates
    // Return 0 when (x,y,z) if out of the pit
    int GetValue2(int x,int y,int z);

    // Add a value at the specified coordinates
    void AddCube(int x,int y,int z);

    // Get "out of bounds" values
    void GetOutOfBounds(int x,int y,int z,int *ox,int *oy,int *oz);

    // Remove full line
    int RemoveLines();

    // Return true if the pit is empty
    BOOL IsEmpty();

  private:
    
    HRESULT CreateGrid(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreateBack(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreateFillingCube(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreatePitLevel(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreateTexturedFillingCube(LPDIRECT3DDEVICE8 pd3dDevice,int i,LPDIRECT3DVERTEXBUFFER8 *vBuffer);
    void SetValue(int x,int y,int z,int value);
    void ChooseMapping(int i,float *sx,float *sy,float *ex,float *ey);
    D3DMATERIAL8 *GetMaterial(int level);
    void RenderCube(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y,int z);
    void RenderEdge(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y,int z,int edge);
    void InitOrderMatrix();
    BOOL IsVisible(int x,int y,int z);
    BOOL IsVisible2(int x,int y,int z);
    BOOL IsLineFull(int z);
    BOOL IsLineEmpty(int z);
    void RemoveLine(int idx);
    void DrawPitLevelCubes(D3DLOCKED_RECT *d3dlr,D3DFORMAT format,int x,int y,int w,int h,DWORD color);

    BOOL inited;
    LPDIRECT3DVERTEXBUFFER8 gridVbf;
    LPDIRECT3DINDEXBUFFER8  gridIdx;
    LPDIRECT3DVERTEXBUFFER8 cubeVbf;
    LPDIRECT3DVERTEXBUFFER8 cubeTVbf[NBTCUBE];
    LPDIRECT3DINDEXBUFFER8  cubeIdx;
    LPDIRECT3DVERTEXBUFFER8 lcubeVbf;
    LPDIRECT3DINDEXBUFFER8  lcubeIdx[12];
    LPDIRECT3DVERTEXBUFFER8 backVbf;
    LPDIRECT3DINDEXBUFFER8  backIdx;
    LPDIRECT3DVERTEXBUFFER8 sideVbf;
    LPDIRECT3DINDEXBUFFER8  sideIdx;
    int pitLevelX;
    int pitLevelY;
    int pitLevelW;
    int pitLevelH;
    D3DFORMAT pitLevelF;
    int width;
    int height;
    int depth;
    int mSize;
    int tcubeIdx;
    int nbGridVertex;
    int nbGridIndex;
    float cubeSide;
    int style;
    D3DMATERIAL8 gridMaterial;
    D3DMATERIAL8 blackMaterial;
    D3DMATERIAL8 darkMaterial;
    D3DMATERIAL8 backMaterial;
    D3DMATERIAL8 backTexMaterial;
    VERTEX origin;
    int *matrix;
    BLOCKITEM *orderMatrix;
    float fWidth;
    float fHeight;
    float fDepth;
    LPDIRECT3DTEXTURE8 backTexture;
    LPDIRECT3DTEXTURE8 cubeTexture;

};

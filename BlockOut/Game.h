/*
 	File:        Game.h
  Description: Game management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#define STRICT
#include <math.h>
#include <D3DX8.h>
#include <D3DApp.h>
#include <D3DFont.h>
#include <D3DUtil.h>
#include <DXUtil.h>
#include <D3DObject.h>
#include "BotPlayer.h"
#include "Sprites.h"
#include "SetupManager.h"
#include "SoundManager.h"

class Game {

  public:
    Game();

    // Set manager
    void SetSetupManager(SetupManager *manager);
    void SetSoundManager(SoundManager *manager);

    // Initialise device objects
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice);

    // Render the Game
    void Render(LPDIRECT3DDEVICE8 pd3dDevice);

    // Release device objects
    void InvalidateDeviceObjects();

    // Start a game
    void StartGame(LPDIRECT3DDEVICE8 pd3dDevice,float fTime);

    // Start a practice
    void StartPractice(LPDIRECT3DDEVICE8 pd3dDevice,float fTime);

    // Start a demo
    void StartDemo(LPDIRECT3DDEVICE8 pd3dDevice,float fTime);

    // Process game
    int Process(BYTE *keys,float fTime);

    // Ask full repaint
    void FullRepaint();

    // Get score
    SCOREREC *GetScore();

    // Set view matrix
    void SetViewMatrix(D3DXMATRIX *mView);

  private:

    // The pit
    Pit thePit;

    // PolyCubes 
    PolyCube allPolyCube[NB_POLYCUBE]; // All polycubes (41 items)
    int   possible[NB_POLYCUBE];       // All possible polycubes for the setup
    int   nbPossible;                  // Number of possible polycube

    // Game sprites
    Sprites sprites;

    // Manager
    SetupManager *setupManager;
    SoundManager *soundManager;

    // Game stuff
    SCOREREC score;     // Score
    int   level;        // Current level
    int   highScore;    // High score of the current game setup
    int   gameMode;     // Game mode
    int   pIdx;         // Current polycube index (in the allPolyCube array)
    int   exitValue;    // Go back to menu when 1
    int   cubePerLevel; // Number of cube per level
    int   dropPos;      // Drop position
    int   cursorPos;    // Descent cursor

    // AI stuff (Demo mode)
    BotPlayer botPlayer;       // AI player
    AI_MOVE AIMoves[MAX_MOVE]; // AI computed moves
    int     nbAIMove;          // Number of AI moves
    int     curAIMove;         // Current AI move
    float   lastAIMoveTime;    // Last move time stamp
    BOOL    demoFlag;          // Demo flag
    BOOL    practiceFlag;      // Practice flag

    // PolyCube coordinates
    D3DXMATRIX  mat;         // Global transform matrix
    D3DXMATRIX  matRot;      // Current rotation matrix (frame)
    D3DXMATRIX  newMatRot;   // Virtual rotation matrix
    D3DXVECTOR3 vPos;        // Current position (frame)
    D3DXVECTOR3 vTransPos;   // Virtual position
    D3DXVECTOR3 vOrgPos;     // Origin of translation
    int         xPos;        // Virtual position
    int         yPos;        // Virtual position
    int         zPos;        // Virtual position

    // Time stamps
    float startTranslateTime;
    float startRotateTime;
    float startRedTime;
    float startPauseTime;
    float startStepTime;
    float startGameTime;
    float startSpark;
    float startEndTime;

    // Animation time
    float animationTime;
    float stepTime;

    // Modes
    int   rotateMode;
    BOOL  dropMode;
    BOOL  dropped;
    BOOL  lastKeyMode;
    BOOL  redMode;

    // Misc
    float curTime;
    int   fullRepaint;
    int   transparent;
    BOOL  inited;
    int   style;
    int   lineWidth;
		BOOL  useAA;
    BOOL  endAnimStarted;

    // Constant matrix
    D3DXMATRIX matRotOx;
    D3DXMATRIX matRotOy;
    D3DXMATRIX matRotOz;
    D3DXMATRIX matRotNOx;
    D3DXMATRIX matRotNOy;
    D3DXMATRIX matRotNOz;

    // Viewport and transformation matrix
    D3DVIEWPORT8 spriteView;
    D3DVIEWPORT8 pitView;
    D3DXMATRIX   matProj;
    D3DXMATRIX   matView;
    D3DXMATRIX   pitMatrix;

    // Background
    D3DObject          background;     // Various resolution
    LPDIRECT3DSURFACE8 backgroundSurf; // 1024*768 technique

    // Spark
    D3DObject          spark;     // Spark

    // Player name font
    CD3DFont *pFont;

		// Help mode
    D3DXMATRIX  matAI;
		float startShowAI;

    // Private methods
    void HandleKey(BYTE *keys);
    BOOL StartRotate(int rType);
    BOOL StartTranslate(int tType);
    void InitTranslate();
    void StartDrop();
    void AddPolyCube();
    void NewPolyCube(BYTE *keys);
    int  SelectPolyCube();
    void TransformCube(D3DXMATRIX *matRot,BLOCKITEM *cubes,int nbCube,int tx,int ty,int tz);
    BOOL IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,int *ox,int *oy,int *oz);
    BOOL IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz);
    BOOL IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,BLOCKITEM *pos);
    BOOL IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,int *ox,int *oy,int *oz,BLOCKITEM *pos);
    BOOL IsLower();
    int  GetBottom();
    HRESULT InitPolyCube(LPDIRECT3DDEVICE8 pd3dDevice,BOOL transparent,float wEgde=0.0f);
    void ComputeScore(int nbLines,BOOL pitEmpty);
    void StartSpark(BLOCKITEM *pos);

};